﻿using System;
using JetBrains.Annotations;

namespace FirstPerson
{
    /// <summary>
    /// Event used for slowing camera and gameobject movements of person.
    /// </summary>
    [PublicAPI]
    public class PlayerSlowedEvent : EventArgs
    {
        public PlayerSlowedEvent(bool isSlowed)
            => IsSlowed = isSlowed;

        public bool IsSlowed { get; }
    }
}