﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FirstPerson.Data
{
    /// <summary>
    /// Holder of player data independent of states as well as states data.
    /// </summary>
    [Serializable]
    public class PersonInfo
    {
        [TabGroup("Movement"), Tooltip("Min time required between jumps, milliseconds"), SerializeField, MinValue(0), LabelText("Jump interval")] 
        private int jumpIntervalMs = 250;

        [TabGroup("Movement"), Tooltip("Movement speed modifier for moving backwards"), SerializeField, Range(0, 1), LabelText("Backwards mod")]
        private float backwardsMod = 0.6f;

        [TabGroup("Movement"), Tooltip("Movement speed modifier for moving sideways"), SerializeField, Range(0, 1), LabelText("Sideways mod")] 
        private float sidewaysMod = 0.8f;

        [TabGroup("Camera"), Tooltip("Camera roll speed around Z-axis"), SerializeField, MinValue(0), LabelText("Camera roll speed")] 
        private float cameraRollSpeed = 2;

        [TabGroup("Camera"), Tooltip("Camera roll interval around Y-axis, degrees"), SerializeField, MinValue(0), LabelText("Camera roll bounds")] 
        private float cameraRollBoundaries = 3;

        [TabGroup("Camera"), Tooltip("X-rotation restriction, angles"), SerializeField, Range(0, 90), LabelText("Max camera angle")] 
        private float maxCameraAngle = 80;

        [TabGroup("Camera"), SerializeField, MinValue(0), LabelText("Default camera height")]
        private float defaultCameraHeight = 1.6f;
        
        [TabGroup("Camera"), Tooltip("Camera movements modifier when player is slowed"), Range(0, 1), SerializeField, LabelText("Camera slow mod")] 
        private float cameraSlowdownMod = 0.2f;

        [TabGroup("Movement"), Tooltip("Player movements modifier when player is slowed"), Range(0, 1), SerializeField, LabelText("Moves slow mod")] 
        private float movementSlowdownMod = 0.2f;

        [BoxGroup("States"), SerializeField]
        private PersonState idle;

        [BoxGroup("States"), SerializeField]
        private PersonState running;

        [BoxGroup("States"), SerializeField]
        private PersonState sprinting;

        internal int JumpIntervalMs => jumpIntervalMs;
        internal float BackwardsMod => backwardsMod;
        internal float SidewaysMod => sidewaysMod;
        internal float CameraRollSpeed => cameraRollSpeed;
        internal float CameraRollBoundaries => cameraRollBoundaries;
        internal float MaxCameraAngle => maxCameraAngle;
        internal float DefaultCameraHeight => defaultCameraHeight;
        internal float CameraSlowdownMod => cameraSlowdownMod;
        internal float MovementSlowdownMod => movementSlowdownMod;

        internal IDictionary<PersonStateType, PersonState> States => new Dictionary<PersonStateType, PersonState>
        {
            {PersonStateType.Idle, idle},
            {PersonStateType.Running, running},
            {PersonStateType.Sprinting, sprinting}
        };
    }
}