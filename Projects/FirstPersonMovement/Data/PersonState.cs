﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

namespace FirstPerson.Data
{
    /// <summary>
    /// Holder of data dependent on player state.
    /// </summary>
    [Serializable]
    public class PersonState
    {
        [TabGroup("Camera"), Tooltip("Camera shake speed along Y-axis"), SerializeField, LabelText("Shake speed"), MinValue(0)]
        private float cameraShakeSpeed;

        [TabGroup("Camera"), Tooltip("Camera shake interval along Y-axis"), SerializeField, LabelText("Shake bounds"), MinValue(0)]
        private float cameraShakeBoundaries;

        [TabGroup("Movement"), SerializeField, LabelText("Speed"), MinValue(0)] 
        private float movementSpeed = 6;

        [TabGroup("Movement"), SerializeField, LabelText("Jump height"), MinValue(0)] 
        private float jumpHeight = 4;

        internal float CameraShakeSpeed => cameraShakeSpeed;
        internal float CameraShakeBoundaries => cameraShakeBoundaries;
        internal float MovementSpeed => movementSpeed;
        internal float JumpHeight => jumpHeight;
    }
}