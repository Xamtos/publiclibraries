﻿namespace FirstPerson.Data
{
    internal enum PersonStateType { Idle, Running, Sprinting }
}