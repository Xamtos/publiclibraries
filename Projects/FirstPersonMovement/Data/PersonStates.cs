﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;
using Utilities.Input;

namespace FirstPerson.Data
{
    internal interface IPersonStates : IDisposable
    {
        PersonState State { get; }
    }

    /// <summary>
    /// States holder subscribed to input events.
    /// </summary>
    internal class PersonStates : IPersonStates
    {
        private static readonly List<Transition<PersonStateType>> Transitions = new List<Transition<PersonStateType>>
        {
            new Transition<PersonStateType>(PersonStateType.Idle, PersonStateType.Running),
            new Transition<PersonStateType>(PersonStateType.Running, PersonStateType.Sprinting),
            new Transition<PersonStateType>(PersonStateType.Running, PersonStateType.Idle),
            new Transition<PersonStateType>(PersonStateType.Sprinting, PersonStateType.Idle)
        };

        private readonly IFiniteStateMachine<PersonStateType> fsm = new FiniteStateMachine<PersonStateType>(PersonStateType.Idle, Transitions);
        private readonly Action toIdle;
        private readonly Action toRun;
        private readonly Action toSprint;
        private readonly IInput input;

        public PersonStates(IDictionary<PersonStateType, PersonState> states, IInput input)
        {
            Validate(states);
            this.input = input;
            
            toIdle = () => State = states[PersonStateType.Idle];
            toRun = () => State = states[PersonStateType.Running];
            toSprint = () => State = states[PersonStateType.Sprinting];
            
            input.Moved += SetRun;
            input.Stopped += SetIdle;
            input.Subscribe<Hold>("Shift", SetSprint);
            input.Subscribe<Release>("Shift", SetIdle);
        }

        public PersonState State { get; private set; }
        
        public void Dispose()
        {
            input.Moved -= SetRun;
            input.Stopped -= SetIdle;
            input.Unsubscribe<Hold>("Shift", SetSprint);
            input.Unsubscribe<Release>("Shift", SetIdle);
        }

        private void SetIdle()
            => fsm.TryTransitToState(PersonStateType.Idle, toIdle);

        private void SetIdle(Release arg)
            => fsm.TryTransitToState(PersonStateType.Idle, toIdle);

        private void SetRun(Vector2 movement)
            => fsm.TryTransitToState(PersonStateType.Running, toRun);

        private void SetSprint(Hold arg)
            => fsm.TryTransitToState(PersonStateType.Sprinting, toSprint);

        private static void Validate(IDictionary<PersonStateType, PersonState> states)
        {
            var values = (PersonStateType[]) Enum.GetValues(typeof(PersonStateType));
            var keys = states.Keys;
            
            if (values.Any(type => !keys.Contains(type)))
                throw new ArgumentOutOfRangeException(nameof(states), "PersonStates dictionary must contain each state.");
        }

    }
}