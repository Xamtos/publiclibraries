﻿using System;
using UnityEngine;
using Utilities;
using Utilities.Input;

namespace FirstPerson
{
    /// <summary>
    /// Class containing camera logic which must be called each Update() and subscribet to input events.
    /// </summary>
    [Serializable]
    internal class CameraAddon : IDisposable
    {
        private readonly IPersonModel model;
        private readonly IInput input;
        
        internal CameraAddon(IPersonModel model, IInput input)
        {
            this.model = model;
            this.input = input;
            input.MouseMoved += CameraRotation;
        }

        public void Dispose()
            => input.MouseMoved -= CameraRotation;

        internal void Update()
            => CameraPosition();

        private void CameraPosition()
        {
            var time = Services.Get<ITime>();
            if (time.Paused) return;

            float speed = model.State.CameraShakeSpeed * time.SinceStartup * model.CameraSensitivity;
            speed = speed < 0 ? 0 : speed;
            float magnitude = model.State.CameraShakeBoundaries;
            magnitude = magnitude <= 0 ? float.Epsilon : magnitude;
            float y = model.Info.DefaultCameraHeight + (float) Math.Sin(speed * Math.PI) * magnitude - magnitude / 2;
            
            Vector3 cameraPosition = model.CameraPosition;
            cameraPosition.y = Mathf.Lerp(cameraPosition.y, y, time.Delta);
            model.CameraPosition = cameraPosition;
        }

        private void CameraRotation(Vector2 mouse)
        {
            var time = Services.Get<ITime>();
            if (time.Paused) return;

            Vector3 cameraRotation = model.CameraRotation.eulerAngles;
            float tilt = -mouse.x * model.Info.CameraRollBoundaries;
            float speed = model.Info.CameraRollSpeed * time.Delta * model.CameraSensitivity;
            cameraRotation.z = Mathf.LerpAngle(model.CameraPosition.z, tilt, speed);

            float x = model.CameraRotation.eulerAngles.x;
            x -= mouse.y * model.CameraSensitivity;
            if (x > 180) x -= 360;
            cameraRotation.x = Mathf.Clamp(x, -model.Info.MaxCameraAngle, model.Info.MaxCameraAngle);
            
            model.CameraRotation = Quaternion.Euler(cameraRotation);
        }
    }
}