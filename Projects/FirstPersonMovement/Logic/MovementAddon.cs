﻿using System;
using UnityEngine;
using Utilities;
using Utilities.Input;
using Debug = System.Diagnostics.Debug;

namespace FirstPerson
{
    /// <summary>
    /// Class containing movement logic subscribed to input events.
    /// </summary>
    [Serializable]
    internal class MovementAddon : IDisposable
    {
        private readonly ITimer timer;
        private readonly IInput input;
        private readonly IPersonModel model;
        
        internal MovementAddon(IPersonModel model, IInput input)
        {
            Debug.Assert(model != null, $"{nameof(model)} is null.");
            timer = new Timer(TimeSpan.FromMilliseconds(model.Info.JumpIntervalMs));
            this.input = input;
            this.model = model;
            input.Subscribe<Click>("Jump", Jump);
            input.Moved += CalculateMovement;
            input.MouseMoved += PlayerRotation;
        }

        public void Dispose()
        {
            input.Unsubscribe<Click>("Jump", Jump);
            input.Moved -= CalculateMovement;
            input.MouseMoved -= PlayerRotation;
        }

        private void CalculateMovement(Vector2 movement)
        {
            var time = Services.Get<ITime>();
            if (time.Paused) return;

            movement *= GetSpeed(movement);
            movement *= time.Delta;
            movement *= model.MovementSensitivity;
            Vector3 result = model.Rotation * new Vector3(movement.x, 0, movement.y);
            result.y = 0;
            result += model.Position;
            model.Position = result;
        }

        private float GetSpeed(Vector2 movement)
        {
            float speed = model.State.MovementSpeed;
            if (movement.y > 0) return speed;
            if (Math.Abs(movement.x) > float.Epsilon) return speed * model.Info.SidewaysMod;
            return speed * model.Info.BackwardsMod;
        }

        private void PlayerRotation(Vector2 mouse)
        {
            Vector3 rotation = model.Rotation.eulerAngles;
            rotation.y += mouse.x * model.CameraSensitivity;
            model.Rotation = Quaternion.Euler(rotation);
        }

        private void Jump(Click arg)
        {
            if (Services.Get<ITime>().Paused) return;

            if (IsGrounded() && timer.ResetIfElapsed())
                model.Force = model.State.JumpHeight * model.MovementSensitivity * Vector3.up;
        }

        private bool IsGrounded()
        {
            // Ray has to have length > 0.5 for some reason.
            const float offset = 0.55f;
            const float radius = 0.5f;
            var ray = new Ray(model.Position + Vector3.up / 2, Vector3.down);
            return Physics.SphereCast(ray, radius, offset, -1, QueryTriggerInteraction.Ignore);
        }
    }
}