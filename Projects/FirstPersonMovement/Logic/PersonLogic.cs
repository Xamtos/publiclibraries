﻿using System;
using Utilities;
using Utilities.Input;

namespace FirstPerson
{
    /// <summary>
    /// Updateable person logic for model's manipulation.
    /// </summary>
    internal interface IPersonLogic : IDisposable
    {
        void Update();
    }
    
    internal class PersonLogic : IPersonLogic
    {
        private readonly CameraAddon camera;
        private readonly MovementAddon movement;
        
        internal PersonLogic(IPersonModel model, IInput input)
        {
            camera = new CameraAddon(model, input);
            movement = new MovementAddon(model, input);
        }
        
        public void Update()
        {
            camera.Update();
        }

        public void Dispose()
        {
            camera.Dispose();
            movement.Dispose();
        }
    }
}