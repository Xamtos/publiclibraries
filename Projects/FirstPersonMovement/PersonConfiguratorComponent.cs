﻿using Events;
using FirstPerson.Data;
using UnityEngine;
using Utilities;
using Utilities.Input;

namespace FirstPerson
{
    /// <summary>
    /// Component used for simple initialization Person's MVC classes.
    /// </summary>
    [DisallowMultipleComponent, RequireComponent(typeof(Rigidbody))]
    internal class PersonConfiguratorComponent : MonoBehaviour
    {
        [SerializeField] private PersonInfo info;
        
        private void Awake()
        {
            IMessenger messenger = Services.Contains<IMessenger>() ? Services.Get<IMessenger>() : EventAggregatorFactory.CreateEmpty();
            IInput input = Services.Contains<IInput>() ? Services.Get<IInput>() : InputBuilder.CreateEmpty();
            var factory = new PersonFactory(messenger, input);
            var view = gameObject.AddComponent<PersonViewWrapper.PersonView>();
            
            factory.Create(info, view);
            Destroy(this);
        }
    }
}