﻿using UnityEngine;

namespace FirstPerson
{
    /// <summary>
    /// Controller used for linking View and Model.
    /// </summary>
    internal interface IPersonController
    {
        // Potential use in future.
    }
    
    internal class PersonController : IPersonController
    {
        private readonly IPersonView view;
        private readonly IPersonModel model;
        private readonly IPersonLogic logic;
        
        public PersonController(IPersonView view, IPersonModel model, IPersonLogic logic)
        {
            this.view = view;
            this.model = model;
            this.logic = logic;

            view.Updated += Update;
            view.Destroyed += OnDestroy;

            model.TransformChanged += TransformChange;
            model.CameraTransformChanged += CameraChange;
            model.ForceChanged += ForceChange;

            Sync();
        }
        
        private void Update()
            => logic.Update();

        private void OnDestroy()
        {
            logic.Dispose();
            model.Dispose();
            view.Dispose();
        }

        private void TransformChange()
            => view.SetTransform(model.Position, model.Rotation);

        private void CameraChange()
            => view.SetCamera(model.CameraPosition, model.CameraRotation);

        private void ForceChange()
        {
            view.AddForce(model.Force);
            model.Force = Vector3.zero;
        }

        private void Sync()
        {
            TransformChange();
            model.CameraPosition = new Vector3(0, model.Info.DefaultCameraHeight, 0);
        }
    }
}