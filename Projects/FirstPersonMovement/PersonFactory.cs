﻿using System;
using Events;
using FirstPerson.Data;
using JetBrains.Annotations;
using Utilities;
using Utilities.Input;

namespace FirstPerson
{
    /// <summary>
    /// Class for Person MVC creation.
    /// </summary>
    [PublicAPI]
    public class PersonFactory
    {
        private IMessenger messenger;
        private IInput input;
        
        public PersonFactory(IMessenger messenger, IInput input)
        {
            Validate(messenger);
            Validate(input);

            this.messenger = messenger;
            this.input = input;
        }

        public IPersonModel Create(PersonInfo info, IPersonView view)
        {
            Validate(info);
            Validate(view);

            var model = new PersonModel(info, input, messenger);
            var logic = new PersonLogic(model, input);
            var _ = new PersonController(view, model, logic);
            return model;
        }

        private void Validate<T>(T argument) where T : class
        {
            if (argument == null)
                throw new ArgumentNullException(typeof(T).ToString());
        }
    }
}