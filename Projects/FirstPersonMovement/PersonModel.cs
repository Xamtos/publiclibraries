﻿using System;
using Events;
using FirstPerson.Data;
using UnityEngine;
using Utilities;
using Utilities.Input;

namespace FirstPerson
{
    /// <summary>
    /// Person's data representation.
    /// </summary>
    public interface IPersonModel : IDisposable
    {
        PersonInfo Info { get; }
        PersonState State { get; }
        Vector3 CameraPosition { get; set; }
        Quaternion CameraRotation { get; set; }
        Vector3 Position { get; set; }
        Quaternion Rotation { get; set; }
        Vector3 Force { get; set; }
        float CameraSensitivity { get; }
        float MovementSensitivity { get; }
        event Action CameraTransformChanged;
        event Action TransformChanged;
        event Action ForceChanged;
    }
    
    internal sealed class PersonModel : IPersonModel
    {
        private readonly IPersonStates states;
        private readonly IMessenger messenger;

        private Vector3 cameraPosition;
        private Quaternion cameraRotation;
        private Vector3 position;
        private Quaternion rotation;
        private Vector3 force;

        internal PersonModel(PersonInfo info, IInput input, IMessenger messenger)
        {
            Info = info;
            this.messenger = messenger;
            states = new PersonStates(info.States, input);
            messenger.Subscribe<PlayerSlowedEvent>(SwitchSlow);
        }

        public event Action CameraTransformChanged;
        public event Action TransformChanged;
        public event Action ForceChanged;

        public PersonInfo Info { get; }

        public PersonState State => states.State;
        
        public Vector3 CameraPosition
        {
            get => cameraPosition;
            set
            {
                if (cameraPosition == value) return;
                    
                cameraPosition = value;
                CameraTransformChanged?.Invoke();
            }
        }

        public Quaternion CameraRotation
        {
            get => cameraRotation;
            set
            {
                if (cameraRotation == value) return;
                    
                cameraRotation = value;
                CameraTransformChanged?.Invoke();
            }
        }

        public Vector3 Position
        {
            get => position;
            set
            {
                if (position == value) return;
                
                position = value;
                TransformChanged?.Invoke();
            }
        }

        public Quaternion Rotation
        {
            get => rotation;
            set
            {
                if (rotation == value) return;
                
                rotation = value;
                TransformChanged?.Invoke();
            }
        }

        public Vector3 Force
        {
            get => force;
            set
            {
                if (force == value || force == Vector3.zero) return;
                
                force = value;
                ForceChanged?.Invoke();
            }
        }

        public float MovementSensitivity { get; private set; }
        public float CameraSensitivity { get; private set; }

        public void Dispose()
        {
            messenger.Unsubscribe<PlayerSlowedEvent>(SwitchSlow);
            CameraTransformChanged = null;
            TransformChanged = null;
            states.Dispose();
        }
        
        private void SwitchSlow(PlayerSlowedEvent args)
        {
            MovementSensitivity = args.IsSlowed ? Info.MovementSlowdownMod : 1;
            CameraSensitivity = args.IsSlowed ? Info.CameraSlowdownMod : 1;
        }
    }
}