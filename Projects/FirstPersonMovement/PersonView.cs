﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

namespace FirstPerson
{
    /// <summary>
    /// Component used for person input/output logic.
    /// </summary>
    public interface IPersonView : IDisposable
    {
        event Action Updated;
        event Action Destroyed;
        void SetTransform(Vector3 position, Quaternion rotation);
        void SetCamera(Vector3 position, Quaternion rotation);
        void AddForce(Vector3 force);
    }

    internal static class PersonViewWrapper
    {
        /// <summary>
        /// MonoBehaviour class representing player view.
        /// </summary>
        [DisallowMultipleComponent, RequireComponent(typeof(Rigidbody))]
        internal sealed class PersonView : MonoBehaviour, IPersonView
        {
            private new Transform camera;
            private new Rigidbody rigidbody;

            public event Action Updated;
            public event Action Destroyed;

            public void SetTransform(Vector3 position, Quaternion rotation)
            {
                transform.rotation = rotation;

                // Resetting horizontal velocity to avoid unexpected movements.
                rigidbody.velocity = new Vector3(0, rigidbody.velocity.y, 0);
                rigidbody.MovePosition(position);
            }

            public void AddForce(Vector3 force)
                => rigidbody.AddForce(force, ForceMode.VelocityChange);

            public void SetCamera(Vector3 position, Quaternion rotation)
                => camera.SetPositionAndRotation(position, rotation);

            private void Awake()
            {
                camera = GetComponentInChildren<Camera>()?.transform;
                rigidbody = GetComponent<Rigidbody>();
                Assert.IsNotNull(camera, "Gameobject must contain an active Camera among child gameObjects.");
            }

            private void Update()
                => Updated?.Invoke();

            private void OnDestroy()
                => Destroyed?.Invoke();

            public void Dispose()
            {
                Updated = null;
                Destroyed = null;
            }
        }
    }
}