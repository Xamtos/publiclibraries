﻿
**MonoBehaviour**:

    - PersonConfiguratorComponent.
    
**Public**:

    - IPersonModel, IPersonView, PersonFactory.

**Notes**:
    
    - Player is slowed via PlayerSlowedEvent event.
    - Attach PersonConfiguratorComponent to player gameobject. Input and Messegner fount automatically via Service Locator.
    - Alternatively use PersonFactory programmatically, providing all dependencies manually.