﻿using JetBrains.Annotations;
using UnityEngine;

namespace Interaction.Addons
{
    /// <summary>
    /// Class for Interactable's animator manipulation.
    /// </summary>
    [CreateAssetMenu(menuName = "Data/Interaction/Addons/Animator")]
    internal class AnimatorAddon : InteractionAddon, IStartAddon
    {
        private enum Type { [UsedImplicitly] StopForever, ResumeIfGracious, ResumeAlways }

        [SerializeField] private Type type = Type.ResumeIfGracious;

        public void OnInteractionStart(InteractableView view) => view.GetComponentInChildren<Animator>().enabled = false;

        public override void OnInteractionEnd(InteractableView view, bool graciously)
            => view.GetComponentInChildren<Animator>().enabled = type == Type.ResumeAlways || type == Type.ResumeIfGracious && graciously;
    }
}