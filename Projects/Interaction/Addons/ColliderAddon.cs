﻿using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace Interaction.Addons
{
    /// <summary>
    /// Class for Interactable collider switching to smooth versions during interaction.
    /// </summary>
    [CreateAssetMenu(menuName = "Data/Interaction/Addons/Colliders")]
    internal class ColliderAddon : InteractionAddon, IStartAddon
    {
        [SerializeField] private bool enableSmoothing;
        [SerializeField] private bool collideWithPlayer;

        private static PhysicMaterial smoothMaterial;

        private void OnEnable()
        {
            smoothMaterial = new PhysicMaterial {dynamicFriction = 0, staticFriction = 0, frictionCombine = PhysicMaterialCombine.Multiply};
        }

        public void OnInteractionStart(InteractableView view)
        {
            var colliders = view.GetComponentsInChildren<Collider>();

            if (enableSmoothing)
                Smooth(colliders, view);

            if (collideWithPlayer || !Services.Contains<IPlayer>()) return;

            SwitchCollisions(colliders, true);
        }

        public override void OnInteractionEnd(InteractableView view, bool graciously)
        {
            var colliders = view.GetComponentsInChildren<Collider>();

            if (enableSmoothing)
                UnSmooth(colliders, view);

            if (collideWithPlayer || !Services.Contains<IPlayer>()) return;

            SwitchCollisions(colliders, false);
        }

        private static void Smooth(Collider[] colliders, InteractableView view)
        {
            var materials = new Dictionary<Collider, PhysicMaterial>();
            foreach (var collider in colliders)
            {
                materials.Add(collider, collider.material);
                collider.material = smoothMaterial;
            }
            view.DataCollection.Add("Materials", materials);
        }

        private static void UnSmooth(Collider[] colliders, InteractableView view)
        {
            view.DataCollection.TryGet("Materials", out Dictionary<Collider, PhysicMaterial> materials);
            foreach (var collider in colliders)
            {
                collider.material = materials[collider];
                materials.Remove(collider);
            }
            view.DataCollection.Remove("Materials");
        }

        private static void SwitchCollisions(Collider[] colliders, bool mode)
        {
            foreach (var col1 in colliders)
            foreach (var col2 in Services.Get<IPlayer>().Rigidbody.GetComponentsInChildren<Collider>())
                Physics.IgnoreCollision(col1, col2, mode);
        }
    }
}