﻿using System.Collections.Generic;
using System.Linq;
using Logging;

// ReSharper disable SuspiciousTypeConversion.Global

namespace Interaction.Addons
{
    internal class AddonsCollection
    {
        private readonly List<InteractionAddon> addons = new List<InteractionAddon>();
        private readonly List<IStartAddon> startAddons = new List<IStartAddon>();
        private readonly List<IUpdateable> updateAddons = new List<IUpdateable>();
        private readonly List<IFixedUpdateable> fixedUpdateAddons = new List<IFixedUpdateable>();
        private readonly List<IUsable> useAddons = new List<IUsable>();
        private readonly List<IThrowable> throwAddons = new List<IThrowable>();
        
        internal AddonsCollection(IEnumerable<InteractionAddon> addonsCollection, string context)
        {
            addons.AddRange(addonsCollection);
            if (!CheckCollection(context)) return;

            RemoveNull();
            RemoveDuplicates(context);
            foreach (InteractionAddon addon in addons)
            {
                PopulateLists(addon);
            }
        }

        private bool CheckCollection(string context)
        {
            if (addons.Count == 0)
            {
                Services.Get<ILog>().Log($"Addons count : 0, object {context}", LogLevel.Warning);
                return false;
            }

            if (addons.Count(a => a is IUpdateable || a is IFixedUpdateable) == 0)
                Services.Get<ILog>()
                    .Log($"Updateable addons not found! Consider adding at least one Updateable addon, object {context}", LogLevel.Warning);
            return true;
        }
        
        private void RemoveDuplicates(string context)
        {
            for (var i = 0; i < addons.Count - 1; i++)
            {
                for (var j = i + 1; j < addons.Count; j++)
                {
                    if (addons[i].GetType() != addons[j].GetType()) continue;
                    
                    Services.Get<ILog>().Log($"Found duplicate addons in object {context}");
                    addons.RemoveAt(j);
                    j--;
                }
            }
        }

        private void RemoveNull()
        {
            for (var i = 0; i < addons.Count; i++)
            {
                if (addons[i] != null) continue;

                addons.RemoveAt(i);
                i--;
            }
        }

        private void PopulateLists(InteractionAddon addon)
        {
            switch (addon)
            {
                case IStartAddon start: startAddons.Add(start);
                    break;
                case IUpdateable update: updateAddons.Add(update);
                    break;
                case IFixedUpdateable fixedUpdate: fixedUpdateAddons.Add(fixedUpdate);
                    break;
                case IUsable use: useAddons.Add(use);
                    break;
                case IThrowable throwAddon: throwAddons.Add(throwAddon);
                    break;
            }
        }

        internal void OnInteractionStart(InteractableView view)
        {
            foreach (var a in startAddons) a.OnInteractionStart(view);
        }
        
        internal void OnInteractionEnd(InteractableView view, bool graciously)
        {
            foreach (var a in addons) a.OnInteractionEnd(view, graciously);
        }
        
        internal void Update(InteractableView view)
        {
            foreach (var a in updateAddons) a.UpdateAddon(view);
        }
        
        internal void FixedUpdate(InteractableView view)
        {
            foreach (var a in fixedUpdateAddons) a.FixedUpdateAddon(view);
        }

        internal void Use(InteractableView view)
        {
            foreach (var a in useAddons) a.Use(view);
        }

        internal void Throw(InteractableView view)
        {
            foreach (var a in throwAddons) a.Throw(view);
        }
    }
}