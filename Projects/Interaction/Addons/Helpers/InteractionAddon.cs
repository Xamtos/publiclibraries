﻿using UnityEngine;

namespace Interaction.Addons
{
    /// <summary>
    /// Class for ScriptableObject addon creation which can be attached to Interactable.
    /// </summary>
    public abstract class InteractionAddon : ScriptableObject
    {
        /// <summary>
        /// Called when interaction is ended.
        /// </summary>
        public abstract void OnInteractionEnd(InteractableView view, bool graciously);
    }
    
    /// <summary>
    /// Addon has OnInteractionStart() method.
    /// </summary>
    public interface IStartAddon
    {
        /// <summary>
        /// Called when interaction is created.
        /// </summary>
        void OnInteractionStart(InteractableView view);
    }
    
    /// <summary>
    /// Addon has Update() method.
    /// </summary>
    public interface IUpdateable
    {
        /// <summary>
        /// Called each Update().
        /// </summary>
        void UpdateAddon(InteractableView view);
    }
    
    /// <summary>
    /// Addon has FixedUpdate() method.
    /// </summary>
    public interface IFixedUpdateable
    {
        /// <summary>
        /// Called each FixedUpdate().
        /// </summary>
        /// <param name="view"></param>
        void FixedUpdateAddon(InteractableView view);
    }

    /// <summary>
    /// Addon has Throw() method.
    /// </summary>
    public interface IThrowable
    {
        /// <summary>
        /// Called when player throws object by activating "Throw" button facing the object.
        /// </summary>
        void Throw(InteractableView view);
    }

    /// <summary>
    /// Addon has Use() method.
    /// </summary>
    public interface IUsable
    {
        /// <summary>
        /// Called when player uses object by activating "Action" button facing the object.
        /// </summary>
        void Use(InteractableView view);
    }

}