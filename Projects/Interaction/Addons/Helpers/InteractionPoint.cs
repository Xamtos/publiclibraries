﻿using System;
using UnityEngine;
using Utilities;
using Utilities.Input;

namespace Interaction.Addons
{
    /// <summary>
    /// Class used to represent interaction point and encapsulate its movement in space.
    /// </summary>
    internal class InteractionPoint : IDisposable
    {
        private readonly InteractionParameters parameters;
        private readonly Transform target;
        private readonly float sizeMod;
        private readonly float angularSpeed;

        private float interactionDistance;
        private Vector3 interactionOffset;

        internal InteractionPoint(Transform target, InteractionParameters parameters, float angularSpeed)
        {
            this.parameters = parameters;
            this.target = target;
            this.angularSpeed = angularSpeed;
            
            Vector3 localScale = target.localScale;
            sizeMod = (localScale.x + localScale.y + localScale.z) / 3;
            
            Ray lookRay = Services.Get<IPlayer>().ForwardRay;
            interactionDistance = lookRay.MeasureRaycastDistance(parameters.MaxInteractionDistance);
            interactionDistance = interactionDistance.Equals(0) ? parameters.MinInteractionDistance : interactionDistance;
            interactionOffset = lookRay.GetPoint(interactionDistance);
            interactionOffset -= target.position;
            interactionOffset -= parameters.TransformOffset;
            Services.Get<IInput>().MouseWheel += CalculateInteractionDistance;
        }

        public void Dispose()
        {
            Services.Get<IInput>().MouseWheel -= CalculateInteractionDistance;
        }
        
        private void CalculateInteractionDistance(float wheel)
        {
            float distance = interactionDistance + wheel;
            distance = Mathf.Clamp(distance, parameters.MinInteractionDistance, parameters.MaxInteractionDistance);
            interactionDistance = distance;
        }

        private Vector3 CalculateInteractionOffset()
        {
            // Interaction point rotation.
            Vector3 offset = target.rotation * parameters.TransformOffset;
            offset *= sizeMod;
            return Vector3.Lerp(interactionOffset, offset, angularSpeed);
        }

        internal void Update()
        {
            interactionOffset = CalculateInteractionOffset();
        }

        /// <summary>
        /// Get current coordinates in world space.
        /// </summary>
        internal Vector3 Get()
        {
            Ray ray = Services.Get<IPlayer>().ForwardRay;
            return ray.GetPoint(interactionDistance) - interactionOffset;
        }

    }
}