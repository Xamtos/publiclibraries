﻿using JetBrains.Annotations;
using UnityEngine;
using Utilities;

namespace Interaction.Addons
{
    /// <summary>
    /// Class for Interactable movements.
    /// </summary>
    [PublicAPI, CreateAssetMenu(menuName = "Data/Interaction/Addons/Movable")]
    public class MovableAddon : InteractionAddon, IFixedUpdateable, IUpdateable, IStartAddon
    {
        private enum RotationType { Vertical, Horizontal, Fixed }

        [SerializeField] private RotationType rotationType = RotationType.Vertical;
        
        /// <summary>
        /// Mass-independent velocity modifier.
        /// </summary>
        [field: Tooltip("Mass-independent velocity modifier."), SerializeField]
        protected float velocityMod = 10;

        /// <summary>
        /// "Mass-independent rotation modifier.
        /// </summary>
        [field: Tooltip("Mass-independent rotation modifier."), SerializeField]
        protected float angularMod = 0.01f;

        /// <inheritdoc />
        public virtual void OnInteractionStart(InteractableView view)
        {
            view.DataCollection.Add("InteractionPoint", new InteractionPoint(view.transform, view.Parameters, angularMod));
            view.Rigidbody.freezeRotation = true;
        }

        /// <inheritdoc />
        public override void OnInteractionEnd(InteractableView view, bool graciously)
        {
            view.DataCollection.TryGet("InteractionPoint", out InteractionPoint point);
            point.Dispose();
            view.DataCollection.Remove("InteractionPoint");
            view.Rigidbody.freezeRotation = false;
        }

        /// <inheritdoc />
        public void UpdateAddon(InteractableView view)
        {
            if (view.Rigidbody.isKinematic) return;

            view.DataCollection.TryGet("InteractionPoint", out InteractionPoint point);
            point.Update();
            Vector3 velocity = point.Get() - view.transform.position;
            view.Rigidbody.velocity = velocity * velocityMod;
        }
        
        /// <inheritdoc />
        public void FixedUpdateAddon(InteractableView view)
        {
            if (view.Rigidbody.isKinematic) return;

            Quaternion quaternion = CalculateRotation(view);
            view.Rigidbody.MoveRotation(Slerp(quaternion, view));
        }

        /// <summary>
        /// Overridable rotation logic calculation.
        /// </summary>
        protected virtual Quaternion CalculateRotation(InteractableView view)
        {
            Vector3 direction = Services.Get<IPlayer>().ForwardRay.direction.normalized;
            switch (rotationType)
            {
                case RotationType.Vertical:
                    direction.y = 0;
                    break;
                case RotationType.Horizontal:
                    direction.x = 0;
                    direction.z = 0;
                    break;
                case RotationType.Fixed: return view.transform.rotation;
            }

            return Quaternion.LookRotation(direction, Vector3.up);
        }

        private Quaternion Slerp(Quaternion desired, InteractableView view)
            => Quaternion.Slerp(view.transform.rotation, desired, angularMod * view.Rigidbody.velocity.sqrMagnitude);
    }
}