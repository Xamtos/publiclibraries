﻿using System.Diagnostics;
using JetBrains.Annotations;
using UnityEngine;
using Utilities;
using Utilities.Input;

namespace Interaction.Addons
{
    [CreateAssetMenu(menuName = "Data/Interaction/Addons/Pushable")]
    internal class PushableAddon : InteractionAddon, IStartAddon
    {
        internal enum PushType { Forward, Up, Sideways, RotateRelativePlayer, RotateRelativeSelf, [UsedImplicitly] Fixed }
        
        [Tooltip("Interactable movement logic: move along transform.forward, transform.up or rotate around X-axis."), SerializeField]
        private PushType verticalPush = PushType.Forward;

        [Tooltip("Interactable movement logic: move along transform.right or rotate around Y-axis."), SerializeField]
        private PushType horizontalPush = PushType.Sideways;
        
        [Tooltip("Mass-independent velocity modifier."), SerializeField]
        protected float velocityMod = 10;

        [Tooltip("Mass-independent rotation modifier."), SerializeField]
        protected float angularMod = 0.01f;

        [Tooltip("Determines whether keep or remove inertia on interaction end."), SerializeField] 
        protected bool preserveInertia = true;
        
        public void OnInteractionStart(InteractableView view)
        {
            Switch(view, true);
        }
        
        public override void OnInteractionEnd(InteractableView view, bool graciously)
        {
            Switch(view, false);
            if (preserveInertia) return;
            
            view.Rigidbody.velocity = Vector3.zero;
            view.Rigidbody.angularVelocity = Vector3.zero;
        }

        private void Switch(InteractableView view, bool mode)
        {
            //TODO: check that closue doesn't screw up unsubscription.
            if (mode)
                Services.Get<IInput>().MouseMoved += Update;
            else
                Services.Get<IInput>().MouseMoved -= Update;


            void Update(Vector2 mouse)
            {
                var player = Services.Get<IPlayer>();
                Vector3 forward = player.ForwardRay.direction.normalized;
                Vector3 up = player.Transform.up.normalized;
                Vector3 right = Vector3.Cross(up, forward).normalized;
                right = right == Vector3.zero ? player.Transform.right : right;

                Vector3 force = Vector3.zero;
                Vector3 torque = Vector3.zero;

                switch (verticalPush)
                {
                    case PushType.Forward:
                        force += mouse.y * forward;
                        break;
                    case PushType.Up:
                        force += mouse.y * up;
                        break;
                    case PushType.RotateRelativePlayer:
                        torque += mouse.y * right;
                        break;
                    case PushType.RotateRelativeSelf:
                        torque += mouse.y * view.transform.right;
                        break;
                }

                switch (horizontalPush)
                {
                    case PushType.RotateRelativePlayer:
                        torque += mouse.x * up;
                        break;
                    case PushType.RotateRelativeSelf:
                        torque += mouse.x * view.transform.up;
                        break;
                    case PushType.Sideways:
                        force += mouse.x * right;
                        break;
                }

                view.Rigidbody.AddForce(force * velocityMod, ForceMode.Acceleration);
                view.Rigidbody.AddTorque(torque * angularMod, ForceMode.Acceleration);
            }
        }
    }
}