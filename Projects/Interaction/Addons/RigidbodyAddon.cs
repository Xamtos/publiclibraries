﻿using UnityEngine;
using Utilities;
using Utilities.Input;

namespace Interaction.Addons
{
    /// <summary>
    /// Class for Interactable's rigidbody manipulation.
    /// </summary>
    [CreateAssetMenu(menuName = "Data/Interaction/Addons/Rigidbody")]
    internal class RigidbodyAddon : InteractionAddon, IStartAddon
    {
        [SerializeField] private bool keepKinematic;
        [SerializeField] private bool removeGravity = true;
        [SerializeField] private bool affectPlayerRigidbodyMass;

        [Tooltip("Mass during interaction applied after mass-dependent modifiers are calculated."), SerializeField]
        private float massDuringInteraction = 0.01f;

        [SerializeField] private float kinematicBreakForce = 10f;

        public void OnInteractionStart(InteractableView view)
        {
            view.Rigidbody.useGravity = !removeGravity;

            view.DataCollection.Add("Mass", view.Rigidbody.mass);
            if (Services.Contains<IPlayer>() && affectPlayerRigidbodyMass) Services.Get<IPlayer>().Rigidbody.mass += view.Rigidbody.mass;

            if (!view.Rigidbody.isKinematic) view.Rigidbody.mass = massDuringInteraction;
            Switch(view, true);
        }

        public override void OnInteractionEnd(InteractableView view, bool graciously)
        {
            view.Rigidbody.useGravity = true;

            view.Rigidbody.isKinematic = graciously && keepKinematic || view.Rigidbody.isKinematic;
            view.DataCollection.TryGet("Mass", out float mass);
            view.Rigidbody.mass = mass;

            view.DataCollection.Remove("Mass");

            if (Services.Contains<IPlayer>() && affectPlayerRigidbodyMass) Services.Get<IPlayer>().Rigidbody.mass -= view.Rigidbody.mass;
            Switch(view, false);
        }

        private void Switch(InteractableView view, bool mode)
        {
            //TODO: check that closue doesn't screw up unsubscription.
            if (mode)
            {
                Services.Get<IInput>().Moved += UpdateAddon;
                Services.Get<IInput>().MouseMoved += UpdateAddon;
            }
            else
            {
                Services.Get<IInput>().Moved -= UpdateAddon;
                Services.Get<IInput>().MouseMoved -= UpdateAddon;
            }
            
            void UpdateAddon(Vector2 magnitude)
            {
                const float tolerance = 0.001f;
                if (!view.Rigidbody.isKinematic) return;

                float force = magnitude.sqrMagnitude * Services.Get<ITime>().Delta * kinematicBreakForce;
                view.Rigidbody.mass -= force;
                if (view.Rigidbody.mass > tolerance) return;

                view.Rigidbody.mass = massDuringInteraction;
                view.Rigidbody.isKinematic = false;
            }
        }
    }
}