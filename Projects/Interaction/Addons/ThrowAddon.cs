﻿using UnityEngine;
using Utilities;

namespace Interaction.Addons
{
    /// <summary>
    /// Class for Interactable's throw logic.
    /// </summary>
    [CreateAssetMenu(menuName = "Data/Interaction/Addons/Rigidbody")]
    internal class ThrowAddon : InteractionAddon, IThrowable
    {
        [Tooltip("Mass-independent push modifier."), SerializeField]
        private float pushForce = 15f;
        [SerializeField] private float randomRotationsMod = 0.1f;

        public override void OnInteractionEnd(InteractableView view, bool graciously)
            => AddRandomRotation(view.Rigidbody);

        public void Throw(InteractableView view)
        {
            Ray lookRay = Services.Get<IPlayer>().ForwardRay;
            Vector3 direction = lookRay.direction.normalized * pushForce;
            Vector3 position = lookRay.GetRaycastHitPoint(view.Parameters.MaxInteractionDistance);
            position = position == Vector3.zero ? view.transform.position : position;
            view.Rigidbody.AddForceAtPosition(direction, position, ForceMode.VelocityChange);
            AddRandomRotation(view.Rigidbody);
        }
        
        private void AddRandomRotation(Rigidbody rb)
        {
            Vector3 magnitude = randomRotationsMod * rb.velocity.magnitude * Random.insideUnitSphere;
            magnitude = Vector3.ClampMagnitude(magnitude, randomRotationsMod);
            rb.AddTorque(magnitude, ForceMode.VelocityChange);
        }

    }
}