﻿using System;

namespace Interaction
{
    public class InteractionEndedEvent : EventArgs
    {
        public InteractionEndedEvent(bool graciously)
        {
            Graciously = graciously;
        }

        public bool Graciously { get; }
    }
}