﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;
using Utilities;

namespace Interaction
{
    /// <summary>
    /// Interactable's parameters holder.
    /// </summary>
    [Serializable]
    public sealed class InteractionParameters
    {
        /// <summary>
        /// Interactable object cannot be moved closer to player than specified value.
        /// </summary>
        [field: HorizontalGroup("Distance"), SerializeField, LabelText("Min"), ValidateInput(nameof(ValidateMinDistance))] 
        public float MinInteractionDistance { get; private set; } = 1.4f;

        /// <summary>
        /// Interaction cannot be started when distance larger than value.
        /// Existing interactions are interrupted upon exceeding value + default offset (1 unit).
        /// </summary>
        [field: HorizontalGroup("Distance"), SerializeField, LabelText("Max"), ValidateInput(nameof(ValidateMaxDistance))] 
        public float MaxInteractionDistance { get; private set; } = 2.4f;

        /// <summary>
        /// Offset from object's origin to desired interaction origin.
        /// </summary>
        [field: Tooltip("Offset from center of object to interaction point."), SerializeField, LabelText("Offset")]
        public Vector3 TransformOffset { get; private set; } = Vector3.zero;

        internal enum InteractionEndType { OnClick, OnRelease }

        [field: SerializeField, LabelText("Interaction end")]
        internal InteractionEndType InteractionEnd { get; private set; } = InteractionEndType.OnClick;

        private bool ValidateMaxDistance(float distance)
            => distance >= MinInteractionDistance && distance > 0;
        
        private bool ValidateMinDistance(float distance)
            => distance <= MaxInteractionDistance && distance >= 0;
    }
}