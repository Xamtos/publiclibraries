﻿using UnityEngine;
using Utilities;

namespace Interaction
{
    /// <summary>
    /// Interface representing Interactable object.
    /// </summary>
    public interface IInteractable
    {
        /// <summary>
        /// Lazily initialized Rigidbody component of Interactable
        /// </summary>
        Rigidbody Rigidbody { get; }
        
        /// <summary>
        /// Data storage for Addons during interactions.
        /// </summary>
        IDataCollection DataCollection { get; }
        
        /// <summary>
        /// Interactable's Parameters container.
        /// </summary>
        InteractionParameters Parameters { get; }
    }
}