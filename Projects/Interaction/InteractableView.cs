﻿using System.Collections;
using System.Collections.Generic;
using Events;
using Interaction.Addons;
using Logging;
using UnityEngine;
using Utilities;

namespace Interaction
{
    /// <summary>
    /// MonoBehaviour class representing Interactable object.
    /// </summary>
    [DisallowMultipleComponent, RequireComponent(typeof(Rigidbody))]
    public sealed class InteractableView : MonoBehaviour, IInteractable
    {
        private enum State { Interacting, Hover, Default }

        //TODO: restrict transitions
        private static readonly IFiniteStateMachine<State> FSM = new FiniteStateMachine<State>(State.Default);
        
        [SerializeField] private InteractionParameters parameters;
        [SerializeField] private List<InteractionAddon> addons;
        
        private new Rigidbody rigidbody;
        private AddonsCollection collection;

        public Rigidbody Rigidbody => rigidbody = rigidbody ? rigidbody : GetComponent<Rigidbody>();

        public IDataCollection DataCollection { get; private set; }
        
        public InteractionParameters Parameters => parameters;

        private void Awake()
        {
            if (!Services.Contains<ILog>()) Services.Register(LoggerBuilder.CreateEmpty());
            if (!Services.Contains<IMessenger>()) Services.Register(EventAggregatorFactory.Create());
            DataCollection = new DataCollection();
            collection = new AddonsCollection(addons, name);
        }
        

        private void OnMouseOver()
        {
            if (OutOfRange() || FSM.IsState(State.Interacting)) return;

            /*if (InputListener.Throw)
                Throw();
            else if (InputListener.Use)
                Use();
            else if (InputListener.Click)
                FSM.TryTransitToState(State.Interacting, () => StartCoroutine(OnInteractionStart()));
            else
                FSM.TryTransitToState(State.Hover, () =>
                {
                    //SetCursorMode(CursorModes.Hover);
                });*/
        }

        private void OnMouseExit()
        {
            if (!FSM.IsState(State.Interacting))
                FSM.TryTransitToState(State.Default, () => { /*SetCursorMode(CursorModes.Locked);*/ });
        }

        private IEnumerator OnInteractionStart()
        {
            //SetCursorMode(CursorModes.Grab);
            yield return null;
            InteractionWrapper.Create(this, collection);
            Services.Get<IMessenger>().Subscribe<InteractionEndedEvent>(OnInteractionEnd);
            enabled = false;
        }

        private void OnInteractionEnd(InteractionEndedEvent args)
        {
            Services.Get<IMessenger>().Unsubscribe<InteractionEndedEvent>(OnInteractionEnd);
            //SetCursorMode(CursorModes.Locked);
            DataCollection.Clear();
            FSM.TryTransitToState(State.Default);
            enabled = true;
        }
        
        private void Throw()
            => collection.Throw(this);

        private void Use()
            => collection.Use(this);

        private bool OutOfRange()
        {
            Vector3 distance = Services.Get<IPlayer>().Transform.position - transform.position;
            return distance.MagnitudeGreaterThan(Parameters.MaxInteractionDistance);
        }
    }
}