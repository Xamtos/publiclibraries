﻿using Events;
using Interaction.Addons;
using JetBrains.Annotations;
using Logging;
using UnityEngine;
using Utilities;

namespace Interaction
{
    // Hides Interaction MonoBehaviour inside the library, forbidding to add it as component manually via inspector.
    [UsedImplicitly]
    internal class InteractionWrapper
    {
        /// <summary>
        /// Create new interaction.
        /// </summary>
        /// <param name="interactableView">Component used in interaction.</param>
        /// <param name="addonsCollection">Addons used in interaction.</param>
        internal static void Create(InteractableView interactableView, AddonsCollection addonsCollection)
        {
            var interaction = new GameObject().AddComponent<Interaction>();
            interaction.Configure(interactableView, addonsCollection);
        }
        
        private class Interaction : MonoBehaviour
        {
            private AddonsCollection addons;
            private InteractableView view;
            
            internal void Configure(InteractableView interactableView, AddonsCollection addonsCollection)
            {
                view = interactableView;
                addons = addonsCollection;
                addons.OnInteractionStart(view);
            }

            private void Update()
            {
                /*if (InputListener.Throw)
                    Throw();

                else if (InputListener.Use)
                    addons.Use(view);

                else if (InputFired)
                    InteractionEnded(true);

                else*/ if (DistanceElapsed() || !view.gameObject.activeSelf)
                    InteractionEnded(false);

                else
                    addons.Update(view);
            }

            private void FixedUpdate()
                => addons.FixedUpdate(view);
            
            private void InteractionEnded(bool graciously)
            {
                Services.Get<ILog>().Log($"InteractionEnded({graciously}).", LogLevel.Info, "Interaction");
                addons.OnInteractionEnd(view, graciously);
                Services.Get<IMessenger>().Trigger(new InteractionEndedEvent(graciously));
                gameObject.SetActive(false);
                Destroy(gameObject);
            }

            private void Throw()
            {
                addons.Throw(view);
                InteractionEnded(false);
            }
            
            private bool DistanceElapsed()
            {
                const float offset = 1f;
                Vector3 distance = Services.Get<IPlayer>().Transform.position - view.transform.position;
                return distance.MagnitudeGreaterThan(view.Parameters.MaxInteractionDistance + offset);
            }

            //private bool InputFired
            //    => InputListener.Release && view.Parameters.InteractionEnd == InteractionParameters.InteractionEndType.OnRelease ||
            //       InputListener.ClickAfterDelay && view.Parameters.InteractionEnd == InteractionParameters.InteractionEndType.OnClick;
        }
    }
}