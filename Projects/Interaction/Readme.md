﻿**Dependencies**:
 
    - Managers. 
    
**MonoBehaviours**:
	
	- InteractableComponent (attach to interactable gameobject).

**ScriptableObjects**:
	
	- AnimatorAddon.
	- ColliderAddon.
	- MovableAddon.
	- PushableAddon.
	- RigidbodyAddon.
	- ThrowAddon.

Attach them to InteractableComponent.

**Publics**:

    - InteractableComponent (main interaction controller)
    - InteractionParameters (interaction parameters with read access for external addons)
    - InteractableDataCollection (data holder for external addons to store temporary data)
    - InteractionAddon, IUpdateable, IFixedUpdateable, IThrowable, IUsable, IStartAddon (inheritable addon interfaces)

**Events**: 
    
    - CursorChangeRequest, 
    - InteractionEnded and InteractionStarted.

