﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Events
{
    internal class EventAggregator : IMessenger
    {
        private readonly Dictionary<Type, Delegate> events = new Dictionary<Type, Delegate>();
        
        public virtual void Subscribe<T>(Action<T> action) where T : EventArgs
        { 
            if (action == null) throw new ArgumentNullException(nameof(action));
            if (IsAnonymous(action)) throw new ArgumentException($"{nameof(action)}: anonymous methods are not allowed.");

            events[typeof(T)] = events.TryGetValue(typeof(T), out Delegate del) ? Delegate.Combine(del, action) : action;
        }

        [SuppressMessage("ReSharper", "DelegateSubtraction")]
        public virtual void Unsubscribe<T>(Action<T> action) where T : EventArgs
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            if (IsAnonymous(action)) throw new ArgumentException($"{nameof(action)}: anonymous methods are not allowed.");
            
            if (events.TryGetValue(typeof(T), out Delegate del)) 
                events[typeof(T)] = Delegate.RemoveAll(del, action);
        }

        public virtual void UnsubscribeAll<T>() where T : EventArgs
            => events[typeof(T)] = null;

        public virtual void Trigger<T>(T args) where T : EventArgs
        {
            if (events.TryGetValue(typeof(T), out Delegate value)) 
                (value as Action<T>)?.Invoke(args);
        }
        
        private static bool IsAnonymous<T>(Action<T> action)
        {
            // TODO: resolve Unity conflict with MethodName definition.
            var invalidChars = new[] {'<', '>'};
            return action.Method.Name.Any(invalidChars.Contains);
        }
    }
}