﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Events
{
    [ExcludeFromCodeCoverage]
    internal class EventAggregatorEmpty : IMessenger
    {
        public void Subscribe<T>(Action<T> action) where T : EventArgs { }
        public void Unsubscribe<T>(Action<T> action) where T : EventArgs { }
        public void UnsubscribeAll<T>() where T : EventArgs { }
        public void Trigger<T>(T args) where T : EventArgs { }
    }
}