﻿using System;
using Logging;

namespace Events
{
    /// <summary>
    /// Factory used for postman creation.
    /// </summary>
    public static class EventAggregatorFactory
    {
        /// <summary>
        /// Create generic IPostman which is supporting provided incident enum types.
        /// </summary>
        public static IMessenger Create() => new EventAggregator();

        /// <summary>
        /// Create logged postman.
        /// </summary>
        public static IMessenger CreateLogged(ILog logger)
        {
            if (logger == null) throw new ArgumentNullException(nameof(logger));
            return new EventAggregatorLogged(logger);
        }

        /// <summary>
        /// Returns empty postman that does nothing.
        /// </summary>
        public static IMessenger CreateEmpty() => new EventAggregatorEmpty();
    }
}