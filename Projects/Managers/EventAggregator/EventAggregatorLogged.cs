﻿using System;
using Logging;

namespace Events
{
    internal class EventAggregatorLogged : EventAggregator
    {
        private readonly ILog logger;

        internal EventAggregatorLogged(ILog logger)
        {
            this.logger = logger;
            logger.Log("Logged EventAggregator created.", LogLevel.Debug, "Events");
        }

        public override void Subscribe<T>(Action<T> action)
        {
            base.Subscribe(action);
            logger.Log($"Subscribed to incident: {typeof(T)}", LogLevel.Debug, "Events");
        }
        
        public override void Unsubscribe<T>(Action<T> action)
        {
            base.Unsubscribe(action);
            logger.Log($"Unsubscribed from incident: {typeof(T)}", LogLevel.Debug, "Events");
        }

        public override void UnsubscribeAll<T>()
        {
            base.UnsubscribeAll<T>();
            logger.Log($"Unsubscribed all from incident: {typeof(T)}", LogLevel.Debug, "Events");
        }

        public override void Trigger<T>(T args)
        {
            logger.Log($"Triggered incident: {typeof(T)}", LogLevel.Debug, "Events");
            base.Trigger(args);
        }
    }
}