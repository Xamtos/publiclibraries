﻿using System;

namespace Events
{
    /// <summary>
    /// Interface providing event subscription/triggering.
    /// </summary>
    public interface IMessenger
    {
        /// <summary> Subscribe new action to provided event type. All subscribed actions are invoked once event is triggered. </summary>
        /// <param name="action">Action to invoke. Anonymous delegates are not allowed.</param>
        /// <exception cref="ArgumentException"></exception>
        void Subscribe<T>(Action<T> action) where T: EventArgs;

        /// <summary> Remove action from subscription on provided event type. </summary>
        /// <param name="action">Action to unsubscribe. Anonymous delegates are not allowed.</param>
        /// <exception cref="ArgumentException"></exception>
        void Unsubscribe<T>(Action<T> action) where T: EventArgs;

        /// <summary>
        /// Remove all subscriptions from provided event type.
        /// </summary>
        void UnsubscribeAll<T>() where T: EventArgs;

        /// <summary>Invoke all subscribed events to provided event type.</summary>
        /// <param name="args">Data passed to each subscriber.</param>
        void Trigger<T>(T args) where T: EventArgs;
    }
}