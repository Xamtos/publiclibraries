﻿**Namespace**: Events.

**Public**: 
    
    - IMessenger interface, 
    - EventAggregatorFactory static.

EventAggregatorFactory returns selected EventAggregator as interface.
Use inheritors of EventArgs for subscription/triggers.