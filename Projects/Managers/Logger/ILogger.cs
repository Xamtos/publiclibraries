﻿namespace Logging
{
    /// <summary>
    /// Interface that represents logger class.
    /// </summary>
    public interface ILog
    {
        /// <summary> Log message using current logger. </summary>
        /// <param name="message">Message to log.</param>
        /// <param name="logLevel">Error - errors only, Warning - errors and warnings, Info - errors, warnings and info. Debug - everything.</param>
        /// <param name="logArea">Messages logged for specified log area only. Use <c>null</c> to log all messages.</param>
        void Log(object message, LogLevel logLevel = LogLevel.Info, string logArea = null);
    }
}