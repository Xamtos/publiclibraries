﻿namespace Logging
{
    /// <summary>
    /// Logging level.
    /// </summary>
    public enum LogLevel
    {
        /// <summary>
        /// All messages including debug messages for more detailed logging.
        /// </summary>
        Debug = 0, 
        /// <summary>
        /// Error, warnings and generic messages.
        /// </summary>
        Info = 1, 
        /// <summary>
        /// Errors and warnings. 
        /// </summary>
        Warning = 2, 
        /// <summary>
        /// Errors only.
        /// </summary>
        Error = 3
    }
}