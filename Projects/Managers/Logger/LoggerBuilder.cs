﻿using System;

namespace Logging
{
    /// <summary>
    /// Logger creating class.
    /// </summary>
    public class LoggerBuilder
    {
        /// <summary>
        /// Create empty logger that does nothing.
        /// </summary>
        public static ILog CreateEmpty() => new EmptyLogger();

        private readonly Logger logger;

        public LoggerBuilder(Action<object> logAction, LogLevel level = LogLevel.Info, string area = null)
        {
            if (logAction == null) throw new ArgumentNullException(nameof(logAction));
            if (!Enum.IsDefined(typeof(LogLevel), level)) throw new ArgumentOutOfRangeException(nameof(level));
            logger = new Logger(logAction, level, area);
        }

        public LoggerBuilder WithError(Action<object> errorAction)
        {
            logger.SetAction(LogLevel.Error, errorAction ?? throw new ArgumentNullException(nameof(errorAction)));
            return this;
        }

        public LoggerBuilder WithWarn(Action<object> warnAction)
        {
            logger.SetAction(LogLevel.Warning, warnAction ?? throw new ArgumentNullException(nameof(warnAction)));
            return this;
        }

        public LoggerBuilder WithDebug(Action<object> debugAction)
        {
            logger.SetAction(LogLevel.Debug, debugAction ?? throw new ArgumentNullException(nameof(debugAction)));
            return this;
        }

        public ILog Create()
            => logger;
    }
}