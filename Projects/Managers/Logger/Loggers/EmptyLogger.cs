﻿using System.Diagnostics.CodeAnalysis;

namespace Logging
{
    [ExcludeFromCodeCoverage]
    internal class EmptyLogger : ILog
    {
        public void Log(object message, LogLevel logLevel = LogLevel.Info, string logArea = null) { }
    }
}