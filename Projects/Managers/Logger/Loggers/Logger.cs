﻿using System;
using System.Collections.Generic;

namespace Logging
{
    /// <summary>
    /// Log to Unity console.
    /// </summary>
    internal class Logger : ILog
    {
        private readonly string area;
        private readonly LogLevel level;
        private readonly Dictionary<LogLevel, Action<object>> actions = new Dictionary<LogLevel, Action<object>>();
        
        internal Logger(Action<object> logAction, LogLevel level, string area = null)
        {
            var levels = (LogLevel[]) Enum.GetValues(typeof(LogLevel));
            foreach (LogLevel lvl in levels) actions[lvl] = logAction;
            
            this.level = level;
            this.area = area ?? string.Empty;
        }

        internal void SetAction(LogLevel logLevel, Action<object> action)
            => actions[logLevel] = action;

        public void Log(object message, LogLevel logLevel = LogLevel.Info, string logArea = null)
        {
            if (!IsCorrectLevel(logLevel)) return;
            
            logArea = logArea ?? string.Empty;
            if (!string.IsNullOrEmpty(area) && !area.Equals(logArea)) return;

            message = $"{(string.IsNullOrEmpty(logArea) ? string.Empty : $"{logArea}: ")}{message}";
            actions[logLevel].Invoke(message);
        }

        private bool IsCorrectLevel(LogLevel logLevel)
            => (int) logLevel >= (int) level && Enum.IsDefined(typeof(LogLevel), logLevel);
    }
}