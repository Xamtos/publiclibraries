﻿**Namespace**: Logging.

**Public**: 
    
    - LogLevel enum, 
    - ILog interface, 
    - LoggerBuilder.

LoggerBuilder creates selected Logger as interface.