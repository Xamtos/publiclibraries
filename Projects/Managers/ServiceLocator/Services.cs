﻿using System;
using System.Collections.Generic;
using System.Linq;

/// <summary>
/// Static Services class used to register and access registered classes.
/// </summary>
public static class Services
{
    private static readonly List<object> RegisteredServices = new List<object>();
    
    // Used for performant lookup through requested Type's interfaces.
    private static readonly Dictionary<Type, Type[]> ServiceInterfaces = new Dictionary<Type, Type[]>();

    /// <summary> Register new class in Services or replace existing one. </summary>
    /// <param name="service">Class to be registered.</param>
    /// <exception cref="ArgumentNullException"></exception>
    public static void Register<T>(T service) where T : class
    {
        if (service == null) throw new ArgumentNullException(nameof(service));

        Type newServiceType = service.GetType();
        var newServiceInterfaces = newServiceType.GetInterfaces();
        
        int existingServiceIndex = !newServiceInterfaces.Any()
            ? RegisteredServices.FindIndex(a => a.GetType() == newServiceType)
            : RegisteredServices.FindIndex(a => SavedInterfacesOfType(a).Intersect(newServiceInterfaces).Any());

        if (existingServiceIndex >= 0)
            RegisteredServices[existingServiceIndex] = service;
        else
            RegisteredServices.Add(service);
        
        ServiceInterfaces[newServiceType] = newServiceInterfaces;
    }

    /// <summary>
    /// Find registered class by type.
    /// </summary>
    /// <exception cref="KeyNotFoundException"></exception>
    public static T Get<T>() where T : class
        => TryGet<T>() ?? throw new KeyNotFoundException($"Type not present in registered services: {typeof(T)}");

    /// <summary>
    /// Check if class of type is registered.
    /// </summary>
    public static bool Contains<T>() where T : class
        => TryGet<T>() != null;

    /// <summary>
    /// Remove all classes from Services.
    /// </summary>
    public static void Clear()
    {
        RegisteredServices.Clear();
        ServiceInterfaces.Clear();
    }

    private static T TryGet<T>() where T : class
    {
        return !typeof(T).IsInterface
            // Find service by type.
            ? RegisteredServices.Find(s => s.GetType() == typeof(T)) as T
            // Find service by interface.
            : RegisteredServices.Find(s => SavedInterfacesOfType(s).Contains(typeof(T))) as T;
    }

    private static Type[] SavedInterfacesOfType(object o)
        => ServiceInterfaces.TryGetValue(o.GetType(), out var interfaces) ? interfaces : Array.Empty<Type>();
}