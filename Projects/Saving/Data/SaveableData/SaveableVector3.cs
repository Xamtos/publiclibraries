﻿using System;
using UnityEngine;

namespace Saving.SaveData
{
    /// <summary>
    /// Easily serializable struct, unlike Unity's Vector3.
    /// </summary>
    [Serializable]
    internal struct SaveableVector3
    {
        private readonly float x, y, z;

        internal SaveableVector3(Vector3 vector)
        {
            x = vector.x;
            y = vector.y;
            z = vector.z;
        }

        internal Vector3 GetVector3() => new Vector3(x, y, z);
    }
}