﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;

namespace Saving.SaveData
{
    /// <summary>
    /// Class for saveable data passing around.
    /// </summary>
    [PublicAPI, Serializable]
    public sealed class SavedObjectData
    {
        private Queue<bool> bools = new Queue<bool>();
        private Queue<float> floats = new Queue<float>();
        private Queue<SaveableVector3> vectors = new Queue<SaveableVector3>();

        /// <summary> Creation of empty data holder. Fill it using Set() methods. </summary>
        /// <param name="id"> Saveable object type unique id. </param>
        public SavedObjectData(int id) => Id = id;

        /// <summary>
        /// Saveable object type unique id.
        /// </summary>
        public int Id { get; }

        /// <summary>
        /// Add vector to serializable collection. Read it in the same order.
        /// </summary>
        public void AddVector(Vector3 value)
            => vectors.Enqueue(new SaveableVector3(value));

        /// <summary>
        /// Read vector from serializable collection.
        /// </summary>
        public Vector3 GetVector()
            => vectors.Dequeue().GetVector3();

        /// <summary>
        /// Add bool to serializable collection. Read it in the same order.
        /// </summary>
        public void AddBool(bool value)
            => bools.Enqueue(value);
        
        /// <summary>
        /// Read bool from serializable collection.
        /// </summary>
        public bool GetBool()
            => bools.Dequeue();

        /// <summary>
        /// Add float to serializable collection. Read it in the same order.
        /// </summary>
        public void AddFloat(float value)
            => floats.Enqueue(value);
        
        /// <summary>
        /// Read float from serializable collection.
        /// </summary>
        public float GetFloat()
            => floats.Dequeue();
    }
}