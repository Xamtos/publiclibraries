﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Logging;
using UnityEngine;

namespace Saving
{
    /// <summary>
    /// Class for loading saveables from resources subfolder.
    /// </summary>
    internal static class SaveablesLoader
    {
        /// <summary> Get saveables' prefubs and ids from folder. </summary>
        /// <param name="path">Relative path to saveables folder inside Resources folder.</param>
        internal static IReadOnlyDictionary<int, GameObject> GetSaveables(string path)
        {
            // TODO: check if possible to async that without breaking Unity.
            var sw = Stopwatch.StartNew();
            sw.Start();
            var objects = Resources.LoadAll(path ?? string.Empty, typeof(GameObject));

            var dictionary = new Dictionary<int, GameObject>();
            foreach (GameObject obj in objects.Cast<GameObject>())
            {
                var saveable = obj.GetComponent<ISaveable>();
                if (saveable == null)
                    Services.Get<ILog>().Log($"Found gameobject without ISaveable component: {obj.name}. Add required component or remove prefab from folder Resources/{path} to avoid overhead.",
                            LogLevel.Warning, "Saving");
                else if (dictionary.ContainsKey(saveable.Id))
                    Services.Get<ILog>().Log($"Found multiple objects with same IDs: {dictionary[saveable.Id].name}{saveable.GameObject.name}", 
                            LogLevel.Error, "Saving");
                else
                    dictionary.Add(saveable.Id, saveable.GameObject);
            }

            sw.Stop();
            Services.Get<ILog>().Log($"Saveables loaded. Time elapsed: {sw.ElapsedMilliseconds} ms. Saveables count: {dictionary.Count}", LogLevel.Debug, "Saving");
            return dictionary;
        }
    }
}