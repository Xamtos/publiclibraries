﻿**Dependencies**: 
    
    - Managers.
    - Utilities.

**Namespace**: Saving.

**Public**:

    - SavedObjectData - holder for save data.
    - SaveDataParser - class containing read/write logic with SavedObjectData. Override it for custom classes saving support.
    - IScenesCollection - interface for scenes management.
    - ScenesCollectionFactory - static class for ScenesCollection creation.

**MonoBehaviours**: 

    - SaveableMonoBehaviour - attach to objects requiring saving.
	
**ScriptableObjects**:

    - SaveDataParser - overrideable class containing read/write logic with SavedObjectData.
    
**How to**:

    1. Attach Saveable script to prefab that needs saving. For each unique prefab set unique id via inspector.
    2. Put all Saveable prefubs into Resources folder or its subfolder.
    3. Create a ScenesCollection via Factory and provide paths to Scenes folder and Saveables subfolder.
    4. Now you can Save, Load and ChangeScene using created collection. All saveable prefabs updated automatically on scene change.
    5. (*Optional*) Override SaveableMonoBehaviour's SetSavedState() and GetSaveData() if you need to transfer additional data. 

Already supported data:

     - Position, 
     - Rotation, 
     - ActiveSelf, 
     - Velocity, 
     - IsKinematic.
     
ISaveablesLinker is registered in Services (internal).
