﻿using Saving.SaveData;
using UnityEngine;

namespace Saving
{
    /// <summary>
    /// Interface representing saveable objects.
    /// </summary>
    internal interface ISaveable
    {
        /// <summary>
        /// Saveable gameobject.
        /// </summary>
        GameObject GameObject { get; }

        /// <summary>
        /// Saveable type unique id.
        /// </summary>
        int Id { get; }

        /// <summary>
        /// Get gameobject data wrapped into SavedObjectData class.
        /// </summary>
        SavedObjectData GetSaveData();

        /// <summary>
        /// Set gameobject data using SavedObjectData wrapper.
        /// </summary>
        void SetSavedState(SavedObjectData state);
    }
}