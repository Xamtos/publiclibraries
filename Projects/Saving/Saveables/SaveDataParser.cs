﻿using System.Collections.Generic;
using JetBrains.Annotations;
using Saving.SaveData;
using UnityEngine;

namespace Saving
{
    [PublicAPI, CreateAssetMenu(menuName = "Saving/SaveDataParser")]
    public class SaveDataParser : ScriptableObject
    {
        public virtual SavedObjectData Export(int id, IEnumerable<Component> components)
        {
            var data = new SavedObjectData(id);
            foreach (var component in components)
            {
                switch (component)
                {
                    case Rigidbody rb: Set(rb, data);
                        break;
                    case Transform transform: Set(transform, data);
                        break;
                }
            }

            return data;
        }

        public virtual void Import(SavedObjectData data, IEnumerable<Component> components)
        {
            foreach (var component in components)
            {
                switch (component)
                {
                    case Rigidbody rb: Get(rb, data);
                        break;
                    case Transform transform: Get(transform, data);
                        break;
                }
            }
        }

        protected virtual void Set(Transform component, SavedObjectData data)
        {
            data.AddBool(component.gameObject.activeSelf);
            data.AddVector(component.position);
            data.AddVector(component.eulerAngles);
            data.AddVector(component.localScale);
        }

        protected virtual void Get(Transform component, SavedObjectData data)
        {
            component.gameObject.SetActive(data.GetBool());
            component.position = data.GetVector();
            component.eulerAngles = data.GetVector();
            component.localScale = data.GetVector();
        }
        
        protected virtual void Set(Rigidbody component, SavedObjectData data)
        {
            data.AddVector(component.velocity);
            data.AddVector(component.angularVelocity);
            data.AddBool(component.isKinematic);
            data.AddFloat(component.mass);
        }
        
        protected virtual void Get(Rigidbody component, SavedObjectData data)
        {
            component.velocity = data.GetVector();
            component.angularVelocity = data.GetVector();
            component.isKinematic = data.GetBool();
            component.mass = data.GetFloat();
        }

    }
}