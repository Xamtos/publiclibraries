﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using Logging;
using Saving.SceneManagement;
using UnityEngine;

namespace Saving.SaveData
{
    /// <summary>
    /// MonoBehaviour class attached to saveable objects. Can be overriden for saving custom data.
    /// </summary>
    [PublicAPI, DisallowMultipleComponent, RequireComponent(typeof(Rigidbody))]
    internal class Saveable : MonoBehaviour, ISaveable
    {
        [SerializeField] private List<Component> componentsForSaving;

        [SerializeField] private SaveDataParser parser;
        [SerializeField] private int id;

        /// <inheritdoc />
        public GameObject GameObject => gameObject;

        /// <inheritdoc />
        
        public int Id => id;

        /// <inheritdoc />
        public SavedObjectData GetSaveData()
            => parser.Export(Id, componentsForSaving);

        /// <inheritdoc />
        public virtual void SetSavedState(SavedObjectData state)
        {
            if (state == null || state.Id == 0) throw new ArgumentException("Saveable.SetSavedState(): argument is not valid.");

            parser.Import(state, componentsForSaving);
        }

        private void Awake()
        {
            Services.Get<ILog>().Log($"Saveable {name} is started.", LogLevel.Debug, "Saving");
            parser = parser ? parser : ScriptableObject.CreateInstance<SaveDataParser>();
            Services.Get<ISaveablesLinker>().Register(this);
        }

        private void OnDestroy()
            => Services.Get<ILog>().Log($"Saveable {name} destroyed.", LogLevel.Debug, "Saving");
    }
}