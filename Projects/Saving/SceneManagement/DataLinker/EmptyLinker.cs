﻿using System.Collections.Generic;
using Saving.SaveData;

namespace Saving.SceneManagement
{
    internal class EmptyLinker : ISaveablesLinker
    {
        public IReadOnlyCollection<SavedObjectData> ExportSaveables() => new List<SavedObjectData>();

        public void Register(ISaveable saveable) { }

        public void Init(IReadOnlyCollection<SavedObjectData> saveables) { }
    }
}