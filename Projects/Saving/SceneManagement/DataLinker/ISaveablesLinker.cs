﻿using System.Collections.Generic;
using Saving.SaveData;

namespace Saving.SceneManagement
{
    /// <summary>
    /// Interface for saveables management in scene.
    /// </summary>
    internal interface ISaveablesLinker
    {
        /// <summary>
        /// Get collection of saveables data from scene.
        /// </summary>
        IReadOnlyCollection<SavedObjectData> ExportSaveables();

        /// <summary>
        /// Add new saveable to scene saveables collection.
        /// </summary>
        void Register(ISaveable saveable);

        /// <summary>Update existing, create missing and delete excessive saveables based on input data.</summary>
        /// <param name="saveables">Saveables data collection to process. <c>null</c> and empty collections are ignored.</param>
        void Init(IReadOnlyCollection<SavedObjectData> saveables);
    }
}