﻿using System.Collections.Generic;
using System.Linq;
using Logging;
using Saving.SaveData;
using UnityEngine;

namespace Saving.SceneManagement
{
    /// <inheritdoc />
    internal class SaveablesLinker : ISaveablesLinker
    {
        private readonly IList<ISaveable> sceneSaveables = new List<ISaveable>();
        private readonly IReadOnlyDictionary<int, GameObject> prefubs;

        internal SaveablesLinker(string saveablesPath) => prefubs = SaveablesLoader.GetSaveables(saveablesPath);

        public IReadOnlyCollection<SavedObjectData> ExportSaveables() => sceneSaveables.Select(s => s.GetSaveData()).ToList().AsReadOnly();

        public void Register(ISaveable saveable) => sceneSaveables.Add(saveable);

        public void Init(IReadOnlyCollection<SavedObjectData> saveables)
        {
            if (saveables?.Any() != true) return;

            var remaining = ConfigureExistingSaveables(saveables);
            InstantiateRemainingSaveables(remaining);

            Services.Get<ILog>().Log("SaveablesLinker initialized.", LogLevel.Info, "Saving");
        }

        
        private IEnumerable<SavedObjectData> ConfigureExistingSaveables(IEnumerable<SavedObjectData> collection)
        {
            var remaining = collection.ToList();
            for (var i = 0; i < sceneSaveables.Count; i++)
            {
                int index = remaining.FindIndex(a => sceneSaveables[i].Id == a.Id);
                if (index != -1)
                {
                    sceneSaveables[i].SetSavedState(remaining[index]);
                    remaining.RemoveAt(index);
                    continue;
                }

                Object.Destroy(sceneSaveables[i].GameObject);
                sceneSaveables.RemoveAt(i);
                i--;
            }

            return remaining;
        }

        private void InstantiateRemainingSaveables(IEnumerable<SavedObjectData> saveables)
        {
            foreach (SavedObjectData saveable in saveables)
            {
                var newObj = Object.Instantiate(prefubs[saveable.Id])?.GetComponent<ISaveable>();
                if (newObj == null) continue;

                newObj.SetSavedState(saveable);
                sceneSaveables.Add(newObj);
                Services.Get<ILog>().Log("New saveable instantiated.", LogLevel.Debug, "Saving");
            }
        }
    }
}