﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logging;
using Utilities;

namespace Saving.SceneManagement
{
    /// <summary>
    /// Class containing saved scenes info and used for scene management.
    /// </summary>
    [Serializable]
    internal class ScenesCollection : IScenesCollection
    {
        private readonly string saveablesPath;
        private readonly string scenesPath;
        private string currentSceneName;

        private List<IScene> scenes = new List<IScene>();

        internal ScenesCollection(string scenesPath, string saveablesPath)
        {
            this.scenesPath = scenesPath;
            this.saveablesPath = saveablesPath;
            currentSceneName = string.Empty;
        }

        /// <inheritdoc />
        public void ChangeScene(string sceneName)
        {
            if (!string.IsNullOrEmpty(currentSceneName)) SaveCurrentScene();
            
            IScene scene = scenes.FirstOrDefault(s => s.Name == sceneName) ?? new WrappedScene(sceneName, scenesPath);
            if (!scenes.Contains(scene))
            {
                Services.Get<ILog>().Log($"Adding new scene to scenes: {sceneName}.", LogLevel.Debug, "Saving");
                scenes.Add(scene);
            }

            currentSceneName = scene.Name;

            var linker = new SaveablesLinker(saveablesPath);
            Services.Register(linker);
            scene.Load();
        }

        /// <inheritdoc />
        public void Load(string path)
        {
            Services.Get<ILog>().Log($"Loading scenes from file: {path}", LogLevel.Info, "Saving");
            var serializer = new Serializer(path);
            if (!serializer.Load(out ScenesCollection collection, out string error))
            {
                Services.Get<ILog>().Log($"Serialization error: {error}", LogLevel.Error, "Saving");
                Services.Get<ILog>().Log("Creating start scene.", LogLevel.Info, "Saving");
                ChangeScene("");
                return;
            }

            scenes = collection.scenes;
            currentSceneName = string.Empty;
            ChangeScene(collection.currentSceneName);
        }

        /// <inheritdoc />
        public void Save(string path)
        {
            if (string.IsNullOrEmpty(currentSceneName))
            {
                Services.Get<ILog>().Log("Attempting to save empty scene collection.", LogLevel.Error);
                return;
            }

            SaveCurrentScene();
            Services.Get<ILog>().Log($"Saving scenes to file: {path}", LogLevel.Info, "Saving");
            var serializer = new Serializer(path);
            if (!serializer.Save(this, out string error)) 
                Services.Get<ILog>().Log($"Serialization exception: {error}", LogLevel.Error);
        }

        private void SaveCurrentScene()
        {
            Services.Get<ILog>().Log($"Saving scene: {currentSceneName}.", LogLevel.Debug, "Saving");
            scenes.Find(s => s.Name == currentSceneName)?.Save();
        }
    }
}