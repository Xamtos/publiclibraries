﻿using JetBrains.Annotations;
using Logging;

namespace Saving.SceneManagement
{
    /// <summary>
    /// Interface used for scene management.
    /// </summary>
    [PublicAPI]
    public interface IScenesCollection
    {
        /// <summary>
        /// Load scenes from file using absolute path and open last scene.
        /// </summary>
        void Load(string path);

        /// <summary>
        /// Save scenes to file using absolute path. Only scenes called by ChangeScene() are saved.
        /// </summary>
        void Save(string path);

        /// <summary>
        /// Save current scene and load saved or default scene by name.
        /// </summary>
        void ChangeScene(string sceneName);
    }

    /// <summary>
    /// ScenesCollection creator.
    /// </summary>
    [PublicAPI]
    public static class ScenesCollectionFactory
    {
        /// <summary>Create new scenes collection.</summary>
        /// <param name="scenesPath">Path scenes loaded from. Relative to 'Assets' folder.</param>
        /// <param name="saveablesPath">Path saveables loaded from. Relative to 'Assets/Resources' folder.</param>
        public static IScenesCollection CreateCollection(string scenesPath, string saveablesPath)
        {
            if (!Services.Contains<ISaveablesLinker>()) Services.Register(new EmptyLinker());
            if (!Services.Contains<ILog>()) Services.Register(LoggerBuilder.CreateEmpty());
            return new ScenesCollection(scenesPath ?? string.Empty, saveablesPath ?? string.Empty);
        }
    }
}