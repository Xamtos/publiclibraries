﻿using System;
using System.Collections.Generic;
using System.Linq;
using Logging;
using Saving.SaveData;
using UnityEngine.SceneManagement;

namespace Saving.SceneManagement
{
    internal interface IScene
    {
        string Name { get; }
        void Load();
        void Save();
    }

    [Serializable]
    internal class WrappedScene : IScene
    {
        private IReadOnlyCollection<SavedObjectData> savedata = new List<SavedObjectData>();

        /// <summary> Create class representing scene from build scenes. </summary>
        /// <param name="sceneName">Name of the scene without path and extension.</param>
        /// <param name="scenesPath">Path to scenes folder.</param>
        internal WrappedScene(string sceneName, string scenesPath)
        {
            Name = sceneName;
            if (!string.IsNullOrEmpty(sceneName) && SceneUtility.GetBuildIndexByScenePath($"{scenesPath}/{sceneName}.unity") >= 0) return;

            Services.Get<ILog>().Log($"There are no scene called {sceneName}.", LogLevel.Warning, "Saving");

            // Getting start scene's name without path and file extension.
            sceneName = SceneUtility.GetScenePathByBuildIndex(1);
            sceneName = sceneName.Split('/').Last();
            Name = sceneName.Substring(0, sceneName.Length - 6);
        }

        public string Name { get; }

        public void Load()
        {
            SceneManager.sceneLoaded += SceneLoaded;
            SceneManager.LoadScene(Name);
        }

        /// <summary>
        /// Save scene data inside this class.
        /// </summary>
        public void Save() => savedata = Services.Get<ISaveablesLinker>().ExportSaveables();

        private void SceneLoaded(Scene scene, LoadSceneMode mode)
        {
            SceneManager.sceneLoaded -= SceneLoaded;
            Services.Get<ISaveablesLinker>().Init(savedata);
            Services.Get<ILog>().Log($"Savedata count: {savedata?.Count ?? 0}.", LogLevel.Debug, "Saving");
        }
    }
}