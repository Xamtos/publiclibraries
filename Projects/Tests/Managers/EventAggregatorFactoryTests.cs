﻿using System;
using Events;
using Logging;
using NSubstitute;
using NUnit.Framework;

namespace Managers
{
    [TestFixture]
    public class EventAggregatorFactoryTests
    {
        [Test]
        public void Factory_Results_NotNull()
        {
            Assert.NotNull(EventAggregatorFactory.CreateEmpty());
            Assert.NotNull(EventAggregatorFactory.CreateLogged(Substitute.For<ILog>()));
            Assert.NotNull(EventAggregatorFactory.Create());
        }
        
        [Test]
        public void Factory_Results_NoThrows()
        {
            Assert.DoesNotThrow(() => EventAggregatorFactory.CreateLogged(Substitute.For<ILog>()));
            Assert.DoesNotThrow(() => EventAggregatorFactory.Create());
            Assert.DoesNotThrow(() => EventAggregatorFactory.CreateEmpty());
        }
        
        [Test]
        public void Factory_Logged_NullArgs_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => EventAggregatorFactory.CreateLogged(null));
        }
    }
}