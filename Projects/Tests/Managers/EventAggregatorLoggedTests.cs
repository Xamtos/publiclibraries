﻿using System;
using Events;
using Logging;
using NSubstitute;
using NUnit.Framework;

namespace Managers
{
    [TestFixture]
    public class EventAggregatorLoggedTests
    {
        private static readonly ILog Logger = Substitute.For<ILog>();
        private static readonly IMessenger Messenger = EventAggregatorFactory.CreateLogged(Logger);

        [SetUp]
        public void SetUp()
        {
            Messenger.UnsubscribeAll<EventArgs>();
            Logger.ClearReceivedCalls();
        }

        [Test]
        public void EventAggregator_Logger_FiredOnSubscription()
        {
            Messenger.Subscribe(Substitute.For<Action<EventArgs>>());
            
            Logger.ReceivedWithAnyArgs().Log(string.Empty);
        }
        
        [Test]
        public void EventAggregator_Logger_FiredOnUnsubscription()
        {
            Messenger.Unsubscribe(Substitute.For<Action<EventArgs>>());
            
            Logger.ReceivedWithAnyArgs().Log(string.Empty);
        }
        
        [Test]
        public void EventAggregator_Logger_FiredOnUnsubscriptionAll()
        {
            Messenger.UnsubscribeAll<EventArgs>();
            
            Logger.ReceivedWithAnyArgs().Log(string.Empty);
        }
        
        [Test]
        public void EventAggregator_Logger_FiredOnTrigger()
        {
            Messenger.Trigger(EventArgs.Empty);
            
            Logger.ReceivedWithAnyArgs().Log(string.Empty);
        }
    }
}