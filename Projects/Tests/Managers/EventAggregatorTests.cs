﻿using System;
using System.ComponentModel.Design;
using System.Diagnostics;
using System.IO;
using Events;
using NSubstitute;
using NUnit.Framework;

namespace Managers
{
    [TestFixture]
    public class EventAggregatorTests
    {
        private static readonly IMessenger Messenger = EventAggregatorFactory.Create();
        private static readonly Action<EventArgs> Action = Substitute.For<Action<EventArgs>>();

        [SetUp]
        public void SetUp()
        {
            Messenger.UnsubscribeAll<EventArgs>();
            Messenger.UnsubscribeAll<TestEventArgs>();
            Action.ClearReceivedCalls();
        }

        [Test, Repeat(5)]
        public void EventAggregator_WrongId_NotFired()
        {
            var incorrectArgs = new ErrorEventArgs(CheckoutException.Canceled);
            
            Messenger.Subscribe(Action);
            Messenger.Trigger(incorrectArgs);
            
            Action.DidNotReceiveWithAnyArgs().Invoke(EventArgs.Empty);
        }
        
        [Test, Repeat(5)]
        public void EventAggregator_CorrectId_Fired()
        {
            Messenger.Subscribe(Action);
            Messenger.Trigger(EventArgs.Empty);
            
            Action.ReceivedWithAnyArgs().Invoke(EventArgs.Empty);
        }
        
        [Test, Repeat(5)]
        public void EventAggregator_EventArgument_TheSame()
        {
            var args = new EventArgs();
            
            Messenger.Subscribe(Action);
            Messenger.Trigger(args);
            
            Action.Received().Invoke(args);
        }
        
        [Test, Repeat(5)]
        public void EventAggregator_LambdaThrows()
        {
            void Anonymous(TestEventArgs a)
            {
            }

            Anonymous(null);
            
            Assert.Throws<ArgumentException>(() => Messenger.Subscribe((Action<TestEventArgs>) Anonymous));
            Assert.Throws<ArgumentException>(() => Messenger.Unsubscribe((Action<TestEventArgs>) Anonymous));
        }
        
        [Test, Repeat(5)]
        public void EventAggregator_MultipleSubscribers_Fired()
        {
            var action2 = Substitute.For<Action<EventArgs>>();

            Messenger.Subscribe(Action);
            Messenger.Subscribe(action2);
            Messenger.Trigger(EventArgs.Empty);
            
            Action.ReceivedWithAnyArgs().Invoke(EventArgs.Empty);
            action2.ReceivedWithAnyArgs().Invoke(EventArgs.Empty);
        }
        
        [Test, Repeat(5)]
        public void EventAggregator_SingleUnsubscription_NotFired()
        {
            Messenger.Subscribe(Action);
            Messenger.Subscribe(Substitute.For<Action<EventArgs>>());
            Messenger.Unsubscribe(Action);
            Messenger.Trigger(EventArgs.Empty);
            
            Action.DidNotReceiveWithAnyArgs().Invoke(EventArgs.Empty);
        }
        
        [Test, Repeat(5)]
        public void EventAggregator_UnsubscriptionAll_NotFired()
        {
            var action2 = Substitute.For<Action<EventArgs>>();

            Messenger.Subscribe(Action);
            Messenger.Subscribe(action2);
            Messenger.UnsubscribeAll<EventArgs>();
            Messenger.Trigger(EventArgs.Empty);
            
            Action.DidNotReceiveWithAnyArgs().Invoke(EventArgs.Empty);
            action2.DidNotReceiveWithAnyArgs().Invoke(EventArgs.Empty);
        }

        [Test, Repeat(5)]
        public void EventAggregator_Profiling()
        {
            Messenger.Subscribe<TestEventArgs>(TestMethod);
            
            var sw = Stopwatch.StartNew();
            var args = new TestEventArgs();
            for (var i = 0; i < 1000000; i++)
            {
                args.Value = i;
                Messenger.Trigger(args);
            }
            sw.Stop();
            Console.WriteLine(sw.ElapsedMilliseconds);
        }

        private void TestMethod(TestEventArgs a)
        {
            float _ = a.Value;
        } 
        
        private class TestEventArgs : EventArgs
        {
            public float Value { get; set; }
        }
    }
}