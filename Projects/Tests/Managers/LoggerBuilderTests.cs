﻿using System;
using System.Diagnostics.CodeAnalysis;
using Logging;
using NSubstitute;
using NUnit.Framework;

namespace Managers
{
    [TestFixture]
    [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
    public class LoggerBuilderTests
    {
        private static readonly Action<object> Action = Substitute.For<Action<object>>();
        private LoggerBuilder builder;

        [SetUp]
        public void SetUp()
        {
            builder = new LoggerBuilder(Action);
            Action.ClearReceivedCalls();    
        }
        
        [Test, Repeat(5)]
        public void LoggerBuilder_DoesNotThrow()
        {
            Assert.DoesNotThrow(() => builder.WithDebug(Action));
            Assert.DoesNotThrow(() => builder.WithError(Action));
            Assert.DoesNotThrow(() => builder.WithWarn(Action));
            Assert.DoesNotThrow(() => builder.Create());
            Assert.DoesNotThrow(() => LoggerBuilder.CreateEmpty());
        }
        
        [Test, Repeat(5)]
        public void LoggerBuilder_Result_NotNull()
        {
            Assert.NotNull(builder.Create());
            Assert.NotNull(LoggerBuilder.CreateEmpty());
        }
        
        [Test, Repeat(5)]
        public void LoggerBuilder_NullArg_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => new LoggerBuilder(null));
            Assert.Throws<ArgumentNullException>(() => builder.WithDebug(null));
            Assert.Throws<ArgumentNullException>(() => builder.WithError(null));
            Assert.Throws<ArgumentNullException>(() => builder.WithWarn(null));
        }
        
        [Test, Repeat(5)]
        public void LoggerBuilder_WrongEnum_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new LoggerBuilder(Action, (LogLevel) (-1)));
        }
    }
}