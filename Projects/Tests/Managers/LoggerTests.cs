﻿using System;
using Logging;
using NSubstitute;
using NUnit.Framework;

namespace Managers
{
    [TestFixture]
    public class LoggerTests
    {
        private static readonly Action<object> Action = Substitute.For<Action<object>>();
        private ILog logger;
        
        [SetUp]
        public void SetUp()
        {
            logger = new LoggerBuilder(Action, LogLevel.Debug).Create();
            Action.ClearReceivedCalls();
        }

        [Test, Repeat(5)]
        [TestCase("")]
        [TestCase("abc")]
        [TestCase("\"some string")]
        public void Logger_SameArgument(string argument)
        {
            logger.Log(argument);

            Action.Received().Invoke(argument);
        }
        
        [Test, Repeat(5)]
        public void Logger_NullArgument_DoesNotThrow()
        {
            Assert.DoesNotThrow(() => logger.Log(null));
        }
        
        [Test, Repeat(5)]
        [TestCase(LogLevel.Debug, LogLevel.Debug)]
        [TestCase(LogLevel.Debug, LogLevel.Info)]
        [TestCase(LogLevel.Debug, LogLevel.Warning)]
        [TestCase(LogLevel.Debug, LogLevel.Error)]
        [TestCase(LogLevel.Info, LogLevel.Info)]
        [TestCase(LogLevel.Info, LogLevel.Warning)]
        [TestCase(LogLevel.Info, LogLevel.Error)]
        [TestCase(LogLevel.Warning, LogLevel.Warning)]
        [TestCase(LogLevel.Warning, LogLevel.Error)]
        [TestCase(LogLevel.Error, LogLevel.Error)]
        public void Logger_CorrectLevels_Invoked(LogLevel initial, LogLevel actual)
        {
            logger = new LoggerBuilder(Action, initial).Create();
            Assert.DoesNotThrow(() => logger.Log(string.Empty, actual));
            Action.ReceivedWithAnyArgs().Invoke(string.Empty);
        }
        
                
        [Test, Repeat(5)]
        [TestCase(LogLevel.Error, LogLevel.Debug)]
        [TestCase(LogLevel.Error, LogLevel.Info)]
        [TestCase(LogLevel.Error, LogLevel.Warning)]
        [TestCase(LogLevel.Warning, LogLevel.Debug)]
        [TestCase(LogLevel.Warning, LogLevel.Info)]
        [TestCase(LogLevel.Info, (LogLevel)(-1))]
        public void Logger_IncorrectLevels_NotInvoked(LogLevel initial, LogLevel actual)
        {
            logger = new LoggerBuilder(Action, initial).Create();
            Assert.DoesNotThrow(() => logger.Log(string.Empty, actual));
            Action.DidNotReceiveWithAnyArgs().Invoke(string.Empty);
        }
        
        [Test, Repeat(5)]
        public void Logger_CorrectArea_Invoked()
        {
            logger = new LoggerBuilder(Action, LogLevel.Info, "a").Create();
            Assert.DoesNotThrow(() => logger.Log(string.Empty, LogLevel.Info, "a"));
            Action.ReceivedWithAnyArgs().Invoke(string.Empty);
        }
        
        [Test, Repeat(5)]
        public void Logger_IncorrectArea_DoesNotInvoke()
        {
            logger = new LoggerBuilder(Action, LogLevel.Info, "a").Create();
            Assert.DoesNotThrow(() => logger.Log(string.Empty, LogLevel.Info, "b"));
            Action.DidNotReceiveWithAnyArgs().Invoke(string.Empty);
        }

    }
}