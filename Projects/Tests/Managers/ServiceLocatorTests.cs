﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Managers
{
    [TestFixture]
    public class ServiceLocatorTests
    {
        [SetUp]
        public void SetUp()
        {
            Services.Clear();
        }
        
        [Test, Repeat(5)]
        public void Services_TwoRegisteredByInterface_Replaced()
        {
            ITestInterface1 testClass1 = new TestClassInheritor1 {ID = 0};
            ITestInterface1 testClass2 = new TestClassInheritor1 {ID = 1};
            ITestInterface2 testClass3 = new TestClassInheritor2();

            Assert.False(Services.Contains<ITestInterface1>());
            Services.Register(testClass1);
            Services.Register(testClass2);
            Services.Register(testClass3);

            Assert.True(Services.Contains<ITestInterface1>());
            Assert.AreEqual(1, Services.Get<ITestInterface1>().ID);
        }
        
        [Test, Repeat(5)]
        public void Services_TwoRegisteredByClass_Replaced()
        {
            var testClass1 = new TestClassNoParents {ID = 0};
            var testClass2 = new TestClassNoParents {ID = 1};

            Assert.False(Services.Contains<TestClassNoParents>());
            Services.Register(testClass1);
            Services.Register(testClass2);
            
            Assert.True(Services.Contains<TestClassNoParents>());
            Assert.AreEqual(1, Services.Get<TestClassNoParents>().ID);
        }
        
        [Test, Repeat(5)]
        public void Services_DifferentClassesSameInterface_Replaced()
        {
            ITestInterface1 testClass1 = new TestClassComplex1 {ID = 1};
            ITestInterface1 testClass2 = new TestClassComplex2 {ID = 2};

            Assert.False(Services.Contains<TestClassComplex1>());
            Assert.False(Services.Contains<TestClassComplex2>());
            Services.Register(testClass1);
            Services.Register(testClass2);
            
            Assert.False(Services.Contains<TestClassComplex1>());
            Assert.True(Services.Contains<TestClassComplex2>());
            Assert.AreEqual(2, Services.Get<ITestInterface1>().ID);
        }

        [Test, Repeat(5)]
        public void Services_Throws()
        {
            Services.Register<ITestInterface2>(new TestClassInheritor2());
            
            Assert.Throws<KeyNotFoundException>(() => Services.Get<TestClassNoParents>());
            Assert.Throws<KeyNotFoundException>(() => Services.Get<ITestInterface1>());
            Assert.Throws<ArgumentNullException>(() => Services.Register<ITestInterface1>(null));
        }
        
        private class TestClassNoParents
        {
            public int ID { get; set; }
        }

        private class TestClassInheritor1 : ITestInterface1
        {
            public int ID { get; set; }
        }
        
        private class TestClassInheritor2 : ITestInterface2 { }

        private interface ITestInterface1 
        { 
            int ID { get; }
        }
        
        private interface ITestInterface2 { }

        private class TestClassComplex1 : TestClassNoParents, ITestInterface1 { }
        private class TestClassComplex2 : TestClassNoParents, ITestInterface1 { }
    }
}