﻿using System;
using System.Collections.Generic;
using NUnit.Framework;

namespace Utilities
{
    [TestFixture]
    public class DataCollectionTests
    {
        private DataCollection data;
        
        [SetUp]
        public void SetUp()
            => data = new DataCollection();

        [TearDown]
        public void TearDown()
            => data.Clear();
        
        [Test]
        public void DataCollection_Retrieved_SameValue()
        {
            const string key = "a";
            const float value = 0;
            data.Add(key, value);
            
            Assert.True(data.TryGet(key, out float result));
            Assert.AreEqual(value, result);
            Assert.AreEqual(value, data.Get<float>(key));
        }

        [Test]
        public void DataCollection_Retrieved_WrongType_Failure()
        {
            const string key = "a";
            const float value = 1;
            data.Add(key, value);
            
            Assert.False(data.TryGet(key, out string result));
            Assert.AreEqual(default(string), result);
            Assert.Throws<ArgumentException>(() => data.Get<string>(key));
        }
        
        [Test]
        public void DataCollection_Removed_Failure()
        {
            const string key = "a";
            const float value = 1;
            data.Add(key, value);
            data.Remove(key);
            
            Assert.False(data.TryGet(key, out float result));
            Assert.AreEqual(default(float), result);
            Assert.Throws<KeyNotFoundException>(() => data.Get<float>(key));
        }
        
        [Test]
        public void DataCollection_DifferentKey_Failure()
        {
            const string keyA = "a";
            const string keyB = "b";
            const float value = 1;
            data.Add(keyA, value);
            
            Assert.False(data.TryGet(keyB, out float result));
            Assert.AreEqual(default(float), result);
            Assert.Throws<KeyNotFoundException>(() => data.Get<float>(keyB));
        }
        
        [Test]
        public void DataCollection_IncorrectArgs_Throws()
        {
           Assert.Throws<ArgumentException>(() => data.Add(string.Empty, 0));
           Assert.Throws<ArgumentException>(() => data.TryGet(string.Empty, out float _));
           Assert.Throws<ArgumentException>(() => data.Remove(string.Empty));
           
           Assert.Throws<ArgumentNullException>(() => data.Add(null, 0));
           Assert.Throws<ArgumentNullException>(() => data.TryGet(null, out float _));
           Assert.Throws<ArgumentNullException>(() => data.Remove(null));
        }
    }
}