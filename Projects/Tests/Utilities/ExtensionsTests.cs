﻿using System;
using System.Linq;
using System.Reflection;
using NUnit.Framework;

namespace Utilities
{
    [TestFixture]
    public class ExtensionsTests
    {
        private static readonly int[] Values = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
        
        [Test, Repeat(5)]
        [TestCase("abc", true)]
        [TestCase("abc123", true)]
        [TestCase("123", true)]
        [TestCase("_123", false)]
        [TestCase("\"abc", false)]
        [TestCase("`", false)]
        [TestCase("", false)]
        [TestCase(null, false)]
        public void LettersAndDigitsOnly_Works(string subject, bool expected)
            => Assert.AreEqual(expected, subject.LettersAndDigitsOnly());

        [Test, Repeat(5)]
        public void RandomElement_ResultInRange()
            => Assert.True(Values.Contains(Values.RandomElement()));

        [Test, Repeat(5)]
        [TestCase(-1)]
        [TestCase(11)]
        public void RandomElements_WrongArg_Throws(int arg)
            => Assert.Throws<ArgumentException>(() => Values.RandomElements(arg));

        [Test, Repeat(5)]
        public void RandomElements_Null_Throws()
        {
            Assert.Throws<ArgumentNullException>(() => ((int[]) null).RandomElements(1));
            Assert.Throws<ArgumentNullException>(() => ((int[]) null).RandomElement());
        }

        [Test, Repeat(5)]
        [TestCase(0), TestCase(1), TestCase(2), TestCase(3), TestCase(4), TestCase(5), TestCase(6), TestCase(7), TestCase(8), TestCase(9)]
        public void RandomElements_ResultsInRange(int count)
        {
            var result = Values.RandomElements(count).ToArray();

            foreach (int i in result) 
                Assert.True(Values.Contains(i));
            
            Assert.AreEqual(count, result.Distinct().Count());
            Assert.AreEqual(count, result.Length);
        }
        
        [Test, Repeat(5)]
        public void AnonymousMethod_ResultsAreCorrect()
        {
            Action anonymous = () => { };
            Action notAnonymous = Console.Clear;

            anonymous();
            
            Assert.True(anonymous.IsAnonymous());
            Assert.False(notAnonymous.IsAnonymous());
        }
        
        [Test, Repeat(5)]
        public void AnonymousMethod_Null_Throws()
        {
            Assert.Throws<ArgumentNullException>(() =>((Action)null).IsAnonymous());
        }

    }
    
}