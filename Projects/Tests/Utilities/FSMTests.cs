﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using NSubstitute;
using NUnit.Framework;

namespace Utilities
{
    [TestFixture]
    public class FSMTests
    {
        private readonly Action substitute = Substitute.For<Action>();
        private FiniteStateMachine<State> fsm;

        [SetUp]
        public void SetUp()
            => fsm = new FiniteStateMachine<State>(State.Inactive);

        [TearDown]
        public void TearDown()
            => substitute.ClearReceivedCalls();

        [Test, Repeat(5)]
        public void FSM_TransitionedStateCheck()
        {
            fsm.TryTransitToState(State.Active);
            
            Assert.False(fsm.IsState(State.Inactive));
            Assert.True(fsm.IsState(State.Active));
        }
        
        [Test, Repeat(5)]
        public void FSM_InitialStateCheck()
        {
            Assert.True(fsm.IsState(State.Inactive));
            Assert.False(fsm.IsState(State.Active));
        }
        
        [Test, Repeat(5)]
        public void FSM_AnotherState_TransitionFired()
        {
            fsm.TryTransitToState(State.Active, substitute.Invoke);
            
            substitute.Received().Invoke();
        }

        [Test, Repeat(5)]
        public void FSM_SameState_TransitionNotFired()
        {
            fsm.TryTransitToState(State.Inactive, substitute.Invoke);
            
            substitute.DidNotReceive().Invoke();
        }
        
        [Test, Repeat(5)]
        public void FSM_TransitionNotSpecified_NotFired()
        {
            fsm = new FiniteStateMachine<State>(State.Inactive, new[] {new Transition<State>(State.Active, State.Inactive)});

            fsm.TryTransitToState(State.Active, substitute.Invoke);
            
            substitute.DidNotReceive().Invoke();
        }
        
        [Test, Repeat(5)]
        public void FSM_TransitionSpecified_Fired()
        {
            fsm = new FiniteStateMachine<State>(State.Inactive, new[] {new Transition<State>(State.Inactive, State.Active)});

            fsm.TryTransitToState(State.Active, substitute.Invoke);
            
            substitute.Received().Invoke();
        }
        
        [Test, Repeat(5)]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void FSM_Creation_Throws()
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => new FiniteStateMachine<State>(State.Inactive, Enumerable.Empty<Transition<State>>()));
            Assert.Throws<ArgumentNullException>(() => new FiniteStateMachine<State>(State.Inactive, null));
            Assert.Throws<ArgumentException>(() => new FiniteStateMachine<TestState>(0, Enumerable.Empty<Transition<TestState>>()));
        }
        
        private enum TestState { }
    }
}