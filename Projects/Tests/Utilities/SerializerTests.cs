﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using NUnit.Framework;

namespace Utilities
{
    [TestFixture]
    public class SerializerTests
    {
        private string path;
        private Serializer serializer;
        
        [SetUp]
        public void SetUp()
        {
            path = $"{TestContext.CurrentContext.TestDirectory}/test/test";
            serializer = new Serializer(path);
        }

        [TearDown]
        public void TearDown()
            => Directory.Delete($"{TestContext.CurrentContext.TestDirectory}/test", true);

        [Test, Repeat(5)]
        public void Serializer_Save_Success()
        {
            var serializable = new Serializable();
            
            Assert.True(serializer.Save(serializable, out string error));
            Assert.AreEqual(string.Empty, error);
        }
        
        [Test, Repeat(5)]
        public void Serializer_Load_Success()
        {
            var serializable = new Serializable {I = 1};
            
            serializer.Save(serializable, out string error);

            Assert.True(serializer.Load(out Serializable obj, out error));
            Assert.AreEqual(string.Empty, error);
            Assert.AreEqual(obj?.I, serializable.I);
        }
        
        [Test, Repeat(5)]
        [SuppressMessage("ReSharper", "ObjectCreationAsStatement")]
        public void Serializer_Constructor_ThrowsNullOrEmpty()
        {
            Assert.Throws<ArgumentException>(() => new Serializer(null));
            Assert.Throws<ArgumentException>(() => new Serializer(string.Empty));
        }
        
        [Test, Repeat(5)]
        public void Serializer_Save_NullOrNotSerializable()
        {
            Assert.False(serializer.Save<Serializable>(null, out string error1));
            Assert.False(serializer.Save(new NonSerializable(), out string error2));
            
            StringAssert.Contains("ArgumentNullException", error1);
            StringAssert.Contains("SerializationException", error2);
        }
        
        [Test, Repeat(5)]
        public void Serializer_Load_FileNotFound()
        {
            Assert.False(serializer.Load<NonSerializable>(out _, out string error));
            
            StringAssert.Contains("FileNotFoundException", error);
        }
        
        [Serializable] 
        private class Serializable { public int I { get; set; } }
        private class NonSerializable { }
    }
}