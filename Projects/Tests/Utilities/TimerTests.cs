﻿using System;
using System.Threading;
using NUnit.Framework;

namespace Utilities
{
    [TestFixture]
    public class TimerTests
    {
        private const int Timeout = 100;
        private const int Tolerance = 1;
        private static readonly Timer Timer = new Timer(TimeSpan.FromMilliseconds(Timeout));

        [SetUp]
        public void SetUp()
        {
            Timer.SetTimeScale(1);
            Timer.Reset();
        }
        
        [Test, Repeat(5)]
        public void Timer_Elapsed()
        {
            Thread.Sleep(Timeout);
            Assert.True(Timer.IsElapsed);
        }
        
        [Test, Repeat(5)]
        public void Timer_Reset_Test()
        {
            Thread.Sleep(Timeout);
            Assert.True(Timer.IsElapsed);
            Timer.Reset();

            Assert.False(Timer.IsElapsed);
        }
        
        [Test, Repeat(5)]
        public void Timer_ResetIfElapsed_Test()
        {
            Thread.Sleep(Timeout);
            Assert.True(Timer.ResetIfElapsed());

            Assert.False(Timer.IsElapsed);
        }
        
        [Test, Repeat(5)]
        public void Timer_ElapsedRemaining()
        {
            const int tolerance = 5;

            Thread.Sleep(Timeout/2);

            double elapsed = Timer.ElapsedTime.TotalMilliseconds;
            double remaining = Timer.RemainingTime.TotalMilliseconds;
            Assert.Less(Timeout - elapsed - remaining, tolerance);
            Assert.Less(remaining - elapsed, tolerance, $"{Timer.ElapsedSeconds}");
        }
        
        [Test, Repeat(5)]
        public void Timer_TimeScale_Half()
        {
            Timer.SetTimeScale(0.5f);
            Thread.Sleep(Timeout);
            Assert.False(Timer.ResetIfElapsed(), $"{Timer.ElapsedTime.TotalMilliseconds:F}");
            Thread.Sleep(Timeout + Tolerance);
            
            Assert.True(Timer.IsElapsed, $"{Timer.ElapsedTime.TotalMilliseconds:F}");
        }
        
        [Test, Repeat(5)]
        public void Timer_TimeScale_Double()
        {
            Timer.SetTimeScale(2);
            Thread.Sleep(Timeout/4);
            Assert.False(Timer.IsElapsed, $"{Timer.ElapsedTime.TotalMilliseconds:F}");
            Thread.Sleep(Timeout/4 + Tolerance);
            
            Assert.True(Timer.IsElapsed, $"{Timer.ElapsedTime.TotalMilliseconds:F}");
        }
        
        [Test, Repeat(5)]
        public void Timer_TimescaleNegative_Throws()
        {
            Assert.Throws<ArgumentException>(() => Timer.SetTimeScale(-1));
        }
    }
}