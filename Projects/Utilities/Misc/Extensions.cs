﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using JetBrains.Annotations;
using UnityEngine;
using Random = System.Random;

namespace Utilities
{
    public static class SharpExtensions
    {
        /// <summary>
        /// Checks if string contains letters and digits only.
        /// </summary>
        public static bool LettersAndDigitsOnly(this string check) 
            => !string.IsNullOrEmpty(check) && !Regex.Match(check, @"[^A-Za-z0-9]+").Success;
        
        /// <summary>
        /// Returns random element of collection. 
        /// </summary>
        /// <exception cref="ArgumentNullException"></exception>
        public static T RandomElement<T>(this IEnumerable<T> collection)
        {
            if (collection == null) 
                throw new ArgumentNullException($"{nameof(RandomElement)}: Collection is null.");
            
            var rnd = new Random();
            T current = default!;
            var count = 0;
            foreach (T element in collection)
            {
                count++;
                if (rnd.Next(0, count) == 0) current = element;
            }
            
            return count != 0 ? current : throw new ArgumentException($"{nameof(RandomElement)}: Collection is empty.");
        }
        
        /// <summary>
        /// Returns collection of random elements from source, with specified elements count. 
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        public static IEnumerable<T> RandomElements<T>(this IEnumerable<T> collection, int count)
        {
            if (count < 0)
                throw new ArgumentException($"{nameof(RandomElements)}: Count must be non-negative number.");
            
            if (collection == null) 
                throw new ArgumentNullException($"{nameof(RandomElements)}: {nameof(collection)} is null");

            if (count == 0)
                return Enumerable.Empty<T>();

            var randomElements = collection.ToList();
            if (randomElements.Count < count)
                throw new ArgumentException($"{nameof(RandomElements)}: Count is greater than collection length.");

            // Remove random elements from collection.
            var rnd = new Random();
            while (randomElements.Count > count) randomElements.RemoveAt(rnd.Next(0, randomElements.Count));
            
            // Rearrange elements order.
            for (int i = randomElements.Count - 1; i > 0; i--)
            {
                int index = rnd.Next(0, i);  
                T value = randomElements[index];  
                randomElements[index] = randomElements[i];  
                randomElements[i] = value;
            }
            
            return randomElements;
        }

        /// <summary>
        /// Checks if argument is anonymous method info.
        /// </summary>
        public static bool IsAnonymous(this Action action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            
            var invalidChars = new[] {'<', '>'};
            return action.Method.Name.Any(invalidChars.Contains);
        }
        
        /// <summary>
        /// Checks if argument is anonymous method info.
        /// </summary>
        public static bool IsAnonymous<T>(this Action<T> action)
        {
            if (action == null) throw new ArgumentNullException(nameof(action));
            
            var invalidChars = new[] {'<', '>'};
            return action.Method.Name.Any(invalidChars.Contains);
        }

    }

    [ExcludeFromCodeCoverage, PublicAPI]
    public static class UnityExtensions
    {
        /// <summary> Fire raycast along the ray with specified distance and layermask. </summary>
        /// <returns>Distance to nearest collider or default value.</returns>
        public static float MeasureRaycastDistance(this Ray ray, float max = Mathf.Infinity, int layerMask = -1) 
            => Physics.Raycast(ray, out RaycastHit hit, max, layerMask, QueryTriggerInteraction.Ignore)
                ? hit.distance
                : default;

        /// <summary> Fire raycast along the ray with specified distance and layermask. </summary>
        /// <returns>Point of RaycastHit or default.</returns>
        public static Vector3 GetRaycastHitPoint(this Ray ray, float maxDistance = Mathf.Infinity, int layerMask = -1)
            => Physics.Raycast(ray, out RaycastHit hit, maxDistance, layerMask, QueryTriggerInteraction.Ignore) 
                ? hit.point 
                : default;

        /// <summary>
        /// Returns true if vector magnitude exceeds provided value.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        public static bool MagnitudeGreaterThan(this Vector3 vector, float max)
        {
            if (max < 0) throw new ArgumentException($"{nameof(MagnitudeGreaterThan)}: magnitude cannot be less than zero.");

            return vector.sqrMagnitude > Mathf.Pow(max, 2);
        }

        /// <summary>
        /// Determines whether there is collider along the ray within specified distance.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        public static bool RaycastInRange(this Ray ray, float range, int layerMask = -1)
        {
            if (range < 0) throw new ArgumentException($"{nameof(RaycastInRange)}: range cannot be less than zero.");

            float result = ray.MeasureRaycastDistance(range, layerMask);
            return result < range && !result.Equals(0);
        }
        
        /// <summary>
        /// Transforms Vector3 to Vector2 in which former z-axis becomes y-axis.
        /// </summary>
        public static Vector2 ToVector2(this Vector3 vector)
            => new Vector2(vector.x, vector.z);
        
        /// <summary>
        /// Transforms Vector2 to Vector3 in which former y-axis becomes z-axis.
        /// </summary>
        /// <param name="vector">Vector to transform.</param>
        /// <param name="height">New y-axis value of Vector3.</param>
        public static Vector3 ToVector3(this Vector2 vector, float height = 0)
            => new Vector3(vector.x, height, vector.y);
    }
}