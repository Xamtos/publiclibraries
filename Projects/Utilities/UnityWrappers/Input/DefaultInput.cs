﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityInput = UnityEngine.Input;

namespace Utilities.Input
{
    /// <summary>
    /// Wrapper is used to prevent component showing up in Unity inspector.
    /// </summary>
    [PublicAPI, ExcludeFromCodeCoverage]
    internal class InputWrapper
    {
        /// <summary>
        /// MonoBehaviour scans input on update and fires linked events
        /// </summary>
        internal class DefaultInput : MonoBehaviour, IInput
        {
            private readonly IFiniteStateMachine<State> fsm = new FiniteStateMachine<State>(State.Inactive);
            private readonly IList<Interaction> interactions = new List<Interaction>();

            internal InputOptions Options { get; set; } = InputOptions.Default;

            public event Action<Vector2> Moved;
            public event Action Stopped;
            public event Action<Vector2> MouseMoved;
            public event Action<float> MouseWheel;

            public void Subscribe<T>(string button, Action<T> action) where T : Interaction
            {
                Debug.Assert(!string.IsNullOrEmpty(button), "Button name is null or empty.");
                Debug.Assert(action != null, "Action is null");

                // Throw if button name is not registered.
                UnityInput.GetButton(button);

                foreach (T interaction in interactions.OfType<T>()) interaction.AddButton(button, action);

            }

            public void Unsubscribe<T>(string button, Action<T> action) where T : Interaction
            {
                Debug.Assert(!string.IsNullOrEmpty(button), "Button name is null or empty.");
                Debug.Assert(action != null, "Action is null.");
                Debug.Assert(!action.IsAnonymous(), "Cannot unsubscribe anonymous methods.");

                foreach (T interaction in interactions.OfType<T>()) interaction.RemoveButton(button, action);
            }

            public void SetActive(bool value)
                => enabled = value;

            public Vector2 GetMousePosition()
                => UnityInput.mousePosition;

            private void Awake()
            {
                interactions.Add(new Click());
                interactions.Add(new Hold());
                interactions.Add(new Release());
                interactions.Add(new DoubleClick(Options.DoubleClickSpeedMs));
                interactions.Add(new HoldRelease(Options.HoldReleaseDurationMs));
            }

            private void Update()
            {
                foreach (Interaction interaction in interactions) interaction.Process();

                Movement();
                Cursor();
                Wheel();
            }

            private void Movement()
            {
                var move = new Vector2(UnityInput.GetAxis(Options.Horizontal), UnityInput.GetAxis(Options.Vertical));
                if (move == Vector2.zero)
                    fsm.TryTransitToState(State.Inactive, Stopped);
                else
                {
                    Moved?.Invoke(move.normalized);
                    fsm.TryTransitToState(State.Active);
                }
            }

            private void Cursor()
            {
                var look = new Vector2(UnityInput.GetAxis(Options.MouseX), UnityInput.GetAxis(Options.MouseY));
                if (look != Vector2.zero) MouseMoved?.Invoke(look.normalized);
            }

            private void Wheel()
            {
                float wheel = UnityInput.GetAxis(Options.ScrollWheel);
                if (Math.Abs(wheel) > Mathf.Epsilon) MouseWheel?.Invoke(wheel);
            }
        }
    }
}