﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;

namespace Utilities.Input
{
    /// <summary>
    /// Empty wrapper with no logic.
    /// </summary>
    [PublicAPI, ExcludeFromCodeCoverage]
    internal class EmptyInput : IInput
    {
        public Vector2 GetMousePosition() => default;
        public event Action<Vector2> Moved;
        public event Action Stopped;
        public event Action<Vector2> MouseMoved;
        public event Action<float> MouseWheel;
        public void Subscribe<T>(string button, Action<T> action) where T : Interaction { }
        public void Unsubscribe<T>(string button, Action<T> action) where T : Interaction { }
        public void SetActive(bool value) { }
    }
}