﻿using System;
using JetBrains.Annotations;
using UnityEngine;

namespace Utilities.Input
{
    /// <summary>
    /// Event-based wrapper for Unity's Input class.
    /// </summary>
    [PublicAPI]
    public interface IInput
    {
        /// <summary>
        /// Get mouse position on the screen as Vector2.
        /// </summary>
        Vector2 GetMousePosition();
        
        /// <summary>
        /// Event fired when movement axes aren't equal to 0. Movement vector is returned.
        /// </summary>
        event Action<Vector2> Moved;
        
        /// <summary>
        /// Event fired when movement axes become 0.
        /// </summary>
        event Action Stopped;
        
        /// <summary>
        /// Event fired when mouse axes aren't equal to 0.
        /// </summary>
        event Action<Vector2> MouseMoved;
        
        /// <summary>
        /// Event fired when mouse scroll wheel axis is not equal to 0.
        /// </summary>
        public event Action<float> MouseWheel;

        /// <summary>
        /// Subscribe to button interaction.
        /// </summary>
        /// <param name="button">Button name.</param>
        /// <param name="action">Callback action.</param>
        void Subscribe<T>(string button, Action<T> action) where T : Interaction;

        /// <summary>
        /// Unsubscribe from button interaction.
        /// </summary>
        /// <param name="button">Button name.</param>
        /// <param name="action">Callback action. Cannot unsubscribe lambda expressions!</param>
        void Unsubscribe<T>(string button, Action<T> action) where T : Interaction;
        
        /// <summary>
        /// Enable or disable input logic execution.
        /// </summary>
        void SetActive(bool value);
    }
}