﻿using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;
using UnityEngine;

namespace Utilities.Input
{
    /// <summary>
    /// Builder for Input wrappers.
    /// </summary>
    [PublicAPI, ExcludeFromCodeCoverage]
    public class InputBuilder
    {
        private const string GameObjectName = "InputService";
        private readonly InputOptions options;

        public InputBuilder(InputOptions options)
            => this.options = options;

        public InputBuilder() : this(InputOptions.Default) { }
        
        /// <summary>
        /// Return default Input wrapper as gameobject with 'Don't Destroy On Load' attribute.
        /// </summary>
        public IInput Create()
        {
            var manager = new GameObject(GameObjectName).AddComponent<InputWrapper.DefaultInput>();
            manager.Options = options;
            Object.DontDestroyOnLoad(manager);
            return manager;
        }

        /// <summary>
        /// Create empty wrapper with no logic attached.
        /// </summary>
        public static IInput CreateEmpty()
            => new EmptyInput();
    }
}