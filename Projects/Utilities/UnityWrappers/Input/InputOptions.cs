﻿using System;
using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;

namespace Utilities.Input
{
    /// <summary>
    /// Struct for customizable parameters for input.
    /// </summary>
    [ExcludeFromCodeCoverage, PublicAPI]
    public struct InputOptions
    {
        private int doubleClickSpeedMs;
        private int holdReleaseDurationMs;
        private string scrollWheel;
        private string mouseY;
        private string mouseX;
        private string vertical;
        private string horizontal;

        public static InputOptions Default { get; } = new InputOptions
        {
            Horizontal = "Horizontal",
            Vertical = "Vertical",
            MouseX = "Mouse X",
            MouseY = "Mouse Y",
            ScrollWheel = "Mouse ScrollWheel",
            DoubleClickSpeedMs = 500,
            HoldReleaseDurationMs = 1000
        };
        
        public string Horizontal
        {
            get => horizontal;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentNullException($"{nameof(Horizontal)}");
                horizontal = value;
            }
        }

        public string Vertical
        {
            get => vertical;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentNullException($"{nameof(Vertical)}");
                vertical = value;
            }
        }

        public string MouseX
        {
            get => mouseX;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentNullException($"{nameof(MouseX)}");
                mouseX = value;
            }
        }

        public string MouseY
        {
            get => mouseY;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentNullException($"{nameof(MouseY)}");
                mouseY = value;
            }
        }

        public string ScrollWheel
        {
            get => scrollWheel;
            set
            {
                if (string.IsNullOrEmpty(value)) throw new ArgumentNullException($"{nameof(ScrollWheel)}");
                scrollWheel = value;
            }
        }

        public int DoubleClickSpeedMs
        {
            get => doubleClickSpeedMs;
            set
            {
                if (value <= 0) throw new ArgumentException($"{nameof(DoubleClickSpeedMs)}");
                doubleClickSpeedMs = value;
            }
        }

        public int HoldReleaseDurationMs
        {
            get => holdReleaseDurationMs;
            set
            {
                if (value <= 0) throw new ArgumentException($"{nameof(HoldReleaseDurationMs)}");
                holdReleaseDurationMs = value;
            }
        }
    }
}