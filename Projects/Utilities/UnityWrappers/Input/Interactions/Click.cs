﻿using System;
using UnityEngine.TestTools;

namespace Utilities.Input
{
    /// <summary>
    /// Interaction is used during the first frame when button is pressed.
    /// </summary>
    [ExcludeFromCoverage]
    public sealed class Click : Interaction
    {
        internal Click() { }

        internal override void Process()
        {
            foreach (var kvp in Buttons)
            {
                if (UnityEngine.Input.GetButtonDown(kvp.Key)) (kvp.Value as Action<Click>)?.Invoke(this);
            }
        }
    }
}