﻿using System;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Utilities.Input
{
    /// <summary>
    /// Interaction is used button is pressed for the second time inside specified time interval.
    /// </summary>
    [ExcludeFromCoverage]
    public sealed class DoubleClick : Interaction
    {
        private readonly IDictionary<string, Timer> clickTimers = new Dictionary<string, Timer>();
        
        internal DoubleClick(int intervalMs)
        {
            TimeSpan interval = TimeSpan.FromMilliseconds(intervalMs);
            ButtonAdded += (sender, name) => clickTimers.Add(name, new Timer(interval));
            ButtonRemoved += (sender, name) => clickTimers.Remove(name);
        }

        internal override void Process()
        {
            foreach (var kvp in clickTimers)
            {

                // Button was not clicked during current frame.
                if (!UnityEngine.Input.GetButtonDown(kvp.Key)) continue;

                if (!kvp.Value.IsElapsed)
                    (Buttons[kvp.Key] as Action<DoubleClick>)?.Invoke(this);
                    
                kvp.Value.Reset();
            }
        }
    }
}