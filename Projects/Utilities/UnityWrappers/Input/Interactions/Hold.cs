﻿using System;
using UnityEngine.TestTools;

namespace Utilities.Input
{
    /// <summary>
    /// Interaction is used each frame while button is pressed.
    /// </summary>
    [ExcludeFromCoverage]
    public sealed class Hold : Interaction
    {
        internal Hold() {}
        
        internal override void Process()
        {
            foreach (var kvp in Buttons)
            {
                if (UnityEngine.Input.GetButton(kvp.Key)) (kvp.Value as Action<Hold>)?.Invoke(this);
            }
        }
    }
}