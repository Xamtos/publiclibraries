﻿using System;
using System.Collections.Generic;
using UnityEngine.TestTools;

namespace Utilities.Input
{
    /// <summary>
    /// Interaction is used during the first frame when button is pressed if button was being held for specified duration.
    /// </summary>
    [ExcludeFromCoverage]
    public sealed class HoldRelease : Interaction
    {
        private readonly IDictionary<string, Timer> holdTimers = new Dictionary<string, Timer>();
        
        internal HoldRelease(int intervalMs)
        {
            TimeSpan interval = TimeSpan.FromMilliseconds(intervalMs);
            ButtonAdded += (sender, name) => holdTimers.Add(name, new Timer(interval));
            ButtonRemoved += (sender, name) => holdTimers.Remove(name);
        }

        internal override void Process()
        {
            foreach (var kvp in holdTimers)
            {
                // Started pressing button.
                if (UnityEngine.Input.GetButtonDown(kvp.Key)) kvp.Value.Reset();

                // Button was released during current frame and was held enough.
                if (UnityEngine.Input.GetButtonUp(kvp.Key) && kvp.Value.IsElapsed) (Buttons[kvp.Key] as Action<HoldRelease>)?.Invoke(this);
            }
        }
    }
}