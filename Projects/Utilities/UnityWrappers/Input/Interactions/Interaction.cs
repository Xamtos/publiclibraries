﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using UnityEngine.TestTools;

namespace Utilities.Input
{
    /// <summary>
    /// Interaction base class.
    /// </summary>
    [ExcludeFromCoverage]
    public abstract class Interaction
    {
        private protected readonly Dictionary<string, Delegate> Buttons = new Dictionary<string, Delegate>();

        private protected event EventHandler<string> ButtonAdded;
        private protected event EventHandler<string> ButtonRemoved;
            
        /// <summary>
        /// Add button and associated action to this interaction.
        /// </summary>
        internal void AddButton<T>(string name, Action<T> action)
        {
            if (!Buttons.TryGetValue(name, out Delegate del)) 
                ButtonAdded?.Invoke(this, name);
            
            Buttons[name] = Delegate.Combine(del, action);
        }

        /// <summary>
        /// Remove button and associated action to this interaction.
        /// </summary>
        [SuppressMessage("ReSharper", "DelegateSubtraction")]
        internal void RemoveButton<T>(string name, Action<T> action)
        {
            if (!Buttons.TryGetValue(name, out Delegate del)) return;

            Buttons[name] = Delegate.RemoveAll(del, action);

            if (Buttons[name] != null) return;

            Buttons.Remove(name);
            ButtonRemoved?.Invoke(this, name);
        }
        
        /// <summary>
        /// Check interaction logic and fire button actions if successful. 
        /// </summary>
        internal abstract void Process();
    }
}