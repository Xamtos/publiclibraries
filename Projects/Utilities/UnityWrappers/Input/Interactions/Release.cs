﻿using System;
using UnityEngine.TestTools;

namespace Utilities.Input
{
    /// <summary>
    /// Interaction is used during the first frame when button is released.
    /// </summary>
    [ExcludeFromCoverage]
    public sealed class Release : Interaction
    {
        internal Release() {}

        internal override void Process()
        {
            foreach (var kvp in Buttons)
            {
                if (UnityEngine.Input.GetButtonUp(kvp.Key)) (kvp.Value as Action<Release>)?.Invoke(this);
            }
        }
    }
}