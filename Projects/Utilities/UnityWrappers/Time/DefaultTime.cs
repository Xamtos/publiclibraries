﻿using System.Diagnostics.CodeAnalysis;

namespace Utilities
{
    [ExcludeFromCodeCoverage]
    internal class DefaultTime : ITime
    {
        public bool Paused { get; } = default;
        public float Delta { get; } = default;
        public float FixedDelta { get; } = default;
        public float SinceStartupReal { get; } = default;
        public float SinceStartup { get; } = default;
        public void SetTimeScale(float value) { }
        public void SetTimeScale(TimeScaleMode value) { }
    }
}