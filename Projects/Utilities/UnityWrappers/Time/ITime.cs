﻿using JetBrains.Annotations;

namespace Utilities
{
    [PublicAPI]
    public interface ITime
    {
        /// <summary>
        /// Checks if timescale equals to 0.
        /// </summary>
        bool Paused { get; }
        
        /// <summary>
        /// Time.deltaTime.
        /// </summary>
        float Delta { get; }
        
        /// <summary>
        /// Time.fixedDeltaTime.
        /// </summary>
        float FixedDelta { get; }
        
        /// <summary>
        /// Time.realtimeSinceStartup. Returns time the game was running in seconds unaffected by timescale.
        /// </summary>
        float SinceStartupReal { get; }
        
        /// <summary>
        /// Time.time. Returns time the game was running in seconds.
        /// </summary>
        float SinceStartup { get; }

        /// <summary> Assign to timescale new value. </summary>
        /// <param name="value"> Value automatically clamped to (0, Max).</param>
        void SetTimeScale(float value);

        /// <summary> Alter timescale value. </summary>
        /// <param name="value"> Timescale mode.</param>
        void SetTimeScale(TimeScaleMode value);
    }
}