﻿using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Builder for Unity.Time wrapper.
    /// </summary>
    [PublicAPI, ExcludeFromCodeCoverage]
    public class TimeBuilder
    {
        public ITime Create()
            => new WrappedTime();

        public ITime CreateDefault()
            => new DefaultTime();
    }
}