﻿namespace Utilities
{
    public enum TimeScaleMode : byte { Paused, Half, Normal, Double }
}