﻿﻿﻿using System;
  using System.Diagnostics.CodeAnalysis;
  using JetBrains.Annotations;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Wrapper for Unity's Time class.
    /// </summary>
    [PublicAPI, ExcludeFromCodeCoverage]
    internal class WrappedTime : ITime
    {
        public bool Paused => Math.Abs(Time.timeScale) < Mathf.Epsilon;

        public float Delta => Time.deltaTime;

        public float FixedDelta => Time.fixedDeltaTime;

        public float SinceStartupReal => Time.realtimeSinceStartup;

        public float SinceStartup => Time.time;

        public void SetTimeScale(float value)
            => Time.timeScale = Mathf.Clamp(value, 0, float.MaxValue);

        public void SetTimeScale(TimeScaleMode value)
            => Time.timeScale = value switch
            {
                TimeScaleMode.Paused => 0,
                TimeScaleMode.Half => 0.5f,
                TimeScaleMode.Normal => 1,
                TimeScaleMode.Double => 2,
                _ => Time.timeScale
            };
    }
}