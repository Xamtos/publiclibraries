﻿using System.Diagnostics.CodeAnalysis;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Operation wrapped inside the class.
    /// </summary>
    [PublicAPI]
    public interface ICommand<in T>
    {
        /// <summary>
        /// Execute operation.
        /// </summary>
        void Execute(T data);
    }

    /// <summary>
    /// Operation wrapped inside the class.
    /// </summary>
    [PublicAPI]
    public interface ICommand
    {
        /// <summary>
        /// Execute operation.
        /// </summary>
        void Execute();
    }

    [ExcludeFromCodeCoverage, PublicAPI]
    public sealed class EmptyCommand<T> : ICommand<T>
    {
        public void Execute(T data) { }
    }
    
    [ExcludeFromCodeCoverage, PublicAPI]
    public sealed class EmptyCommand : ICommand
    {
        public void Execute() { }
    }
}