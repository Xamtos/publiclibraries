﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Collection that support data saving and retrieving.
    /// </summary>
    [PublicAPI]
    public interface IDataCollection
    {
        /// <summary>
        /// Add data to container.
        /// </summary>
        /// <param name="key">This value will be used to retrieve data.</param>
        /// <param name="data">Data to add.</param>
        void Add<T>(string key, T data) where T : notnull;
        
        /// <summary>
        /// Remove data from container.
        /// </summary>
        /// <param name="key">Data name to remove.</param>
        void Remove(string key);
        
        /// <summary>
        /// Get data from container.
        /// </summary>
        /// <param name="key">Key to retrieve.</param>
        T Get<T>(string key);
        
        /// <summary>
        /// Safely get data from container.
        /// </summary>
        /// <param name="key">Key to retrieve.</param>
        /// <param name="value">Retrieved data.</param>
        bool TryGet<T>(string key, out T value);
        
        /// <summary>
        /// Remove all data from container.
        /// </summary>
        void Clear();
    }
    
    /// <summary>
    /// Class that support data saving and retrieving.
    /// </summary>
    [Serializable]
    public sealed class DataCollection : IDataCollection
    {
        private readonly Dictionary<string, DataHolder> objects = new Dictionary<string, DataHolder>();
        
        /// <inheritdoc />
        public void Add<T>(string key, T data) where T: notnull
        {
            ValidateKey(key);
            objects[key] = new DataHolder(typeof(T), data);
        }

        /// <inheritdoc />
        public void Remove(string key)
        {
            ValidateKey(key);
            if (objects.ContainsKey(key)) objects.Remove(key);
        }

        /// <inheritdoc />
        public bool TryGet<T>(string key, out T value)
        {
            ValidateKey(key);
            if (objects.TryGetValue(key, out DataHolder data) && data.Type == typeof(T))
            {
                value = (T)data.Data;
                return true;
            }

            value = default;
            return false;
        }
        
        /// <inheritdoc />
        public T Get<T>(string key)
        {
            ValidateKey(key);
            if (!objects.TryGetValue(key, out DataHolder data)) 
                throw new KeyNotFoundException($"{nameof(DataCollection)}: key '{key}' not found.");

            if (data.Type != typeof(T))
                throw new ArgumentException(
                    $"{nameof(DataCollection)}: attempting to retrieve value of different type. Stored type: {data.Type}, requested: {typeof(T)}");

            return (T) data.Data;
        }

        /// <inheritdoc />
        public void Clear()
            => objects.Clear();

        [UsedImplicitly]
        private void ValidateKey(string key)
        {
            if (key == null) throw new ArgumentNullException($"{nameof(DataCollection)}: key argument is null.");
            if (key == string.Empty) throw new ArgumentException($"{nameof(DataCollection)}: key argument is empty.");
        }
        
        /// <summary>
        /// Immutable container for boxed data.
        /// </summary>
        private struct DataHolder
        {
            public DataHolder(Type type, object data)
            {
                Type = type;
                Data = data;
            }

            public Type Type { get; }
            public object Data { get; }
        }
    }
}