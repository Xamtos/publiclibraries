﻿using System;
using System.Collections.Generic;
using System.Linq;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Default states.
    /// </summary>
    [PublicAPI]
    public enum State { Active = 1, Inactive = 0 }

    /// <summary>
    /// Finite State Machine used to invoke specified action on state change.
    /// </summary>
    public interface IFiniteStateMachine<in T> where T : Enum
    {
        /// <summary> Invoke action if state transition was successful. Transition is possible to different state only.</summary>
        /// <param name="newState"> State FSM will attempt to transit to. </param>
        /// <param name="onTransit"> Action to invoke on successful transition. </param>
        /// <returns> Transition success. </returns>
        bool TryTransitToState(T newState, [CanBeNull] Action onTransit = null);
        
        /// <summary>
        /// Check if FSM current state equals provided one.
        /// </summary>
        bool IsState(T check);
    }
    
    /// <summary>
    /// Class to invoke specified action on state change.
    /// </summary>
    [PublicAPI]
    public class FiniteStateMachine<T> : IFiniteStateMachine<T> where T : Enum
    {
        // Using List<> to avoid allocations.
        private readonly List<Transition<T>> transitions;
        private T state;

        /// <summary>
        /// Create new FSM with specified starting state and available transitions.
        /// </summary>
        /// <exception cref="ArgumentException"></exception>
        /// <exception cref="ArgumentNullException"></exception>
        /// <exception cref="ArgumentOutOfRangeException"></exception>
        public FiniteStateMachine(T state, IEnumerable<Transition<T>> transitions)
        {
            var values = (T[]) Enum.GetValues(typeof(T));
            if (values.Length <= 1) throw new ArgumentException($"{typeof(T)} must contain at least two elements to use finite state machine with it.");
            if (transitions == null) throw new ArgumentNullException($"{nameof(transitions)} is null.");

            this.state = state;
            this.transitions = transitions.ToList();
            if (!this.transitions.Any()) throw new ArgumentOutOfRangeException(nameof(transitions), "Unable to create finite state machine without transitions.");
        }
        
        /// <summary>
        /// Create new FSM with specified starting state and all transitions enabled.
        /// </summary>
        public FiniteStateMachine(T state) : this(state, Transition<T>.CreateAll().ToList()) { }

        /// <inheritdoc />
        public bool TryTransitToState(T newState, Action onTransit = null)
        {
            if (IsState(newState)) return false;

            // Not using LINQ to avoid allocations.
            foreach (var a in transitions)
            {
                if (!Compare(a.From, state) || !Compare(a.To, newState)) continue;

                state = newState;
                onTransit?.Invoke();
                return true;
            }

            return false;
        }
        
        /// <inheritdoc />
        public bool IsState(T check)
            => Compare(state, check);

        private bool Compare(T val1, T val2)
            => EqualityComparer<T>.Default.Equals(val1, val2);
    }
}