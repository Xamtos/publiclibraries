﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Transition wrapper for specified enum.
    /// </summary>
    [PublicAPI]
    public struct Transition<T> where T : Enum
    {
        public Transition(T from, T to)
        {
            From = from;
            To = to;
        }

        public T From { get; }
        public T To { get; }

        /// <summary>
        /// Return all available transitions for each enum value.
        /// </summary>
        public static IEnumerable<Transition<T>> CreateAll()
        {
            var values = (T[]) Enum.GetValues(typeof(T));
            for (var i = 0; i < values.Length; i++)
            {
                for (var j = 0; j < values.Length; j++)
                {
                    if (i == j) continue;
                    
                    yield return new Transition<T>(values[i], values[j]);
                }
            }
        }
    }
}