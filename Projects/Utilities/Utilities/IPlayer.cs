﻿using JetBrains.Annotations;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Rigidbody object state wrapper.
    /// </summary>
    [PublicAPI]
    public interface IPlayer
    {
        Transform Transform { get; }
        Rigidbody Rigidbody { get; }

        /// <summary>
        /// Current look direction.
        /// </summary>
        Ray ForwardRay { get; }
    }
}