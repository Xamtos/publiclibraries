﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Serialization and deserialization of generic object.
    /// </summary>
    [PublicAPI]
    public interface ISerializer
    {
        /// <summary> Serialize object to a file. </summary>
        /// <param name="objToSerialize">Object for serialization. Must be marked <c>[System.Serializable]</c>.</param>
        /// <param name="error">Exception text if occured.</param>
        bool Save<T>(T objToSerialize, out string error);
        
        /// <summary> Deserialize object of type T from a file. </summary>
        /// <param name="obj">Deserialized object.</param>
        /// <param name="error">Exception text if occured.</param>
        bool Load<T>([CanBeNull] out T obj, out string error);
    }

    /// <inheritdoc />
    [PublicAPI]
    public class Serializer : ISerializer
    {
        private readonly string path;
        
        /// <summary>
        /// Create serializer with specified absolute file path to serialize/deserialize.
        /// </summary>
        public Serializer(string path)
        {
            this.path = path;
            
            string name = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(name)) 
                throw new ArgumentException($"{nameof(Serializer)}: incorrect path.");
                
            if (!Directory.Exists(name))
                Directory.CreateDirectory(name);
        }

        /// <inheritdoc />
        public bool Save<T>(T objToSerialize, out string error)
        {
            try
            {
                using Stream stream = File.Open(path, FileMode.Create);
                var bin = new BinaryFormatter();
                bin.Serialize(stream, objToSerialize);
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                return false;
            }
        }

        /// <inheritdoc />
        public bool Load<T>(out T obj, out string error)
        {
            try
            {
                using Stream stream = File.Open(path, FileMode.Open);
                var bin = new BinaryFormatter();
                obj = (T)bin.Deserialize(stream);
                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                obj = default;
                error = ex.ToString();
                return false;
            }
        }
    }
}