﻿﻿using System;
using JetBrains.Annotations;

namespace Utilities
{
    /// <summary>
    /// Simple timer to track time intervals affected by timescale.
    /// </summary>
    [PublicAPI]
    public interface ITimer
    {
        /// <summary>
        /// Time elapsed since Timer was created or reset.
        /// </summary>
        TimeSpan ElapsedTime { get; }
        
        /// <summary>
        /// Time elapsed since Timer was created or reset in seconds.
        /// </summary>
        float ElapsedSeconds { get; }
        
        /// <summary>
        /// Checks if provided time is elapsed.
        /// </summary>
        bool IsElapsed { get; }
        
        /// <summary>
        /// Time remaining before elapsing.
        /// </summary>
        TimeSpan RemainingTime { get; }
        
        /// <summary>
        /// IsElapsed check and subsequent timer reset if result is true.
        /// </summary>
        bool ResetIfElapsed();
        
        /// <summary>
        /// Reset elapsed time to zero.
        /// </summary>
        void Reset();
        
        /// <summary>
        /// Set timer speed.
        /// </summary>
        void SetTimeScale(float timescale);
    }

    /// <inheritdoc />
    [PublicAPI]
    public class Timer : ITimer
    {
        private const float Tolerance = 0.01f;

        private readonly TimeSpan threshold;
        private DateTime origin;
        private float currentTimescale = 1;

        /// <summary>
        /// Create a timer with specified interval to track.
        /// </summary>
        public Timer(TimeSpan timeSpan)
        {
            origin = DateTime.Now;
            threshold = timeSpan;
        }

        /// <inheritdoc />
        public TimeSpan ElapsedTime => 
            Math.Abs(currentTimescale - 1) < Tolerance 
                ? DateTime.Now - origin 
                : TimeSpan.FromMilliseconds((DateTime.Now - origin).TotalMilliseconds * currentTimescale);

        /// <inheritdoc />
        public float ElapsedSeconds => (float) ElapsedTime.TotalSeconds;

        /// <inheritdoc />
        public bool IsElapsed => threshold < ElapsedTime;
        
        /// <inheritdoc />
        public TimeSpan RemainingTime
        {
            get
            {
                TimeSpan result = threshold - ElapsedTime;
                return result.TotalSeconds < 0 ? TimeSpan.Zero : result;
            }
        }

        /// <inheritdoc />
        public bool ResetIfElapsed()
        {
            if (!IsElapsed) return false;

            Reset();
            return true;
        }
        
        /// <inheritdoc />
        public void Reset()
            => origin = DateTime.Now;
        
        /// <inheritdoc />
        public void SetTimeScale(float timescale)
        {
            if (timescale <= 0)
                throw new ArgumentException("Timer: timescale must be positive number.");

            currentTimescale = timescale;
        }
    }
}