# Libraries:

Referenced UnityEngine.dll, UnityEditor.dll and Sirenix.OdinInspector.Attributes.dll are removed from repository! Use your own while rebuilding solution.

* **[Managers](https://bitbucket.org/Xamtos/publiclibraries/src/master/Projects/Managers/).** 
Library containing managers:
    - Logger.
	- ServiceLocator.
	- EventAggregator.
	
* **[Utilities](https://bitbucket.org/Xamtos/publiclibraries/src/master/Projects/Utilities/).**
Various wrappers, extensions, attributes, generic classes.

* **[First Person Movement](https://bitbucket.org/Xamtos/publiclibraries/src/master/Projects/FirstPersonMovement/).**
MVC-based components for first-person character control.

* **[Tests](https://bitbucket.org/Xamtos/publiclibraries/src/master/Projects/Tests/).**
Unit tests.